<?php
	$uploads_dir = "../static/";

	if (getimagesize($_FILES['image']['tmp_name']) && $_FILES['image']['error'] == 0) {
    	$tmp_name = $_FILES["image"]["tmp_name"];
        $name = basename($_FILES["image"]["name"]);
        move_uploaded_file($tmp_name, "$uploads_dir/$name");

        $arr = array('status' => 'success',
        	'msg' => 'gambar berhasil di upload',
        	'url' => "http://localhost:8000/static/$name");

    } else {
    	$arr = array('status' => 'fail', 'msg' => 'gambar tidak dapat diupload');
    }
    echo json_encode($arr, JSON_UNESCAPED_SLASHES);

?>
