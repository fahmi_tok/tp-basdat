
<?php

class DBConnection {

	// comment this in production
	const HOST = "localhost";
	const USER = "postgres";
	const PATH = "tokokeren";
	const PASS = "";

	private $dbconn;
	private $dbschema;

	function __construct() {
		if (!$this->dbconn = pg_connect("host=".self::HOST." user=".self::USER ." password=".self::PASS))
		die("Couldn't connect to server");
		$this->transform_query_data(pg_query( $this->dbconn, "set search_path to tokokeren;"));
		if( !$this->dbschema = pg_query( $this->dbconn, "set search_path to ".self::PATH))
		die("Couldn't connect to databases");
		$this->dbschema = pg_query( $this->dbconn, "set datestyle to European;");
	}

	private function transform_query_data($rawData) {
		$rows = array();
		while($row = pg_fetch_assoc($rawData)) {
		    $rows[] = $row;
		}

		$ret = array('data' => $rows);
		return json_encode($ret);
	}

	public function select($table, $whereCondition, $addCondition=null) {
		$queryString = "select * from ".$table." where";
		$conds = json_decode($whereCondition);
		foreach($conds as $key => $cond) {
			$queryString.=" ".$key."='".$cond."'";
			if(next($conds)){
				$queryString.=" and";
			}
		}
		$addConds = json_decode($addCondition);
		if (is_array($addConds) || is_object($addConds)){
			foreach($addConds as $key => $cond) {
				$queryString.=" ".$key." ".$cond;
				if(next($conds)){
					$queryString.=" ";
				}
			}
		}
		$queryString.=";";
		if( $res = pg_query( $this->dbconn, $queryString))
			return $this->transform_query_data($res);
	}

	public function selectIn($table, $field, $inArray) {
		$queryString = "select * from ".$table." where " . $field . " in (";
		foreach($inArray as $key => $elem) {
			$queryString.="'".$elem."'";
			if(next($inArray)){
				$queryString.=", ";
			}
		}
		$queryString.=");";

		if( $res = pg_query( $this->dbconn, $queryString))
			return $this->transform_query_data($res);
	}

	public function selectLast($table, $field, $whereCondition=null) {
		$queryString = "select max(".$field.") as ".$field." from ".$table;
		if(!is_null($whereCondition)) {
			$queryString.=" where";
		}
		$inArray = json_decode($whereCondition);
		foreach($inArray as $key => $elem) {
			$queryString.="'".$elem."'";
			if(next($inArray)){
				$queryString.=", ";
			}
		}
		$queryString.=";";

		if( $res = pg_query( $this->dbconn, $queryString))
			return $this->transform_query_data($res);
	}

	public function delete($table, $whereCondition) {
		$queryString = "delete from ".$table." where";
		$conds = json_decode($whereCondition);
		foreach($conds as $key => $cond) {
			$queryString.=" ".$key."='".$cond."'";
			if(next($conds)){
				$queryString.=" and";
			}
		}
		$queryString.=";";

		if( $res = pg_query( $this->dbconn, $queryString))
			return $this->transform_query_data($res);
	}

	public function update($table, $setCondition, $whereCondition) {
		$queryString = "update ".$table." set";
		$sets = json_decode($setCondition);
		foreach ($sets as $key => $set) {
			$queryString.=" ".$key."='".$set."'";
			if(next($sets)){
				$queryString.=",";
			}
		}
		$conds = json_decode($whereCondition);
		$queryString.=" where";
		foreach($conds as $key => $cond) {
			$queryString.=' '.$key."='".$cond."'";
			if(next($conds)){
				$queryString.=" and";
			}
		}
		$queryString.=';';

		if( $res = pg_query( $this->dbconn, $queryString))
			return $this->transform_query_data($res);
	}

	public function insert($table, $condition) {
		$queryString = "insert into ".$table;
		$columns = " (";
		$values = " values (";
		$conds = json_decode($condition);
		foreach($conds as $key => $cond) {
			$columns.=$key;
			$values.="'".$cond."'";
			if(next($conds)){
				$columns.=",";
				$values.=",";
			}
		}
		$columns.=")";
		$values.=")";
		$queryString.=$columns.$values.";";
		if( $res = pg_query( $this->dbconn, $queryString))
			return $this->transform_query_data($res);
	}
}
?>
