
<?php
require 'DB.php';

// $arr = array('no_invoice' => 'INV0000000', 'kode_produk' => 'P0000000');
// 	$json_arr = json_encode($arr);
// echo $db->select('list_item', $json_arr);

const PAGE_LIMIT = 10;


function login($db, $post){
	$email = $post['email'];
	$password = $post['password'];

	$arr = array('email' => $email, 'password' => $password);
	$json_arr = json_encode($arr);
	$query = json_decode($db->select('pengguna', $json_arr));

	$retArray = array();
	if(sizeof($query->{'data'}) == 1) {

		$role = '';
		$arr = array('email' => $email);
		$json_arr = json_encode($arr);
		$query = json_decode($db->select('pelanggan', $json_arr));
		if(sizeof($query->{'data'}) == 1) {
			if($query->{'data'}[0]->{'is_penjual'} == 't') $role = 'penjual';
			else $role = 'pembeli';
		}
		else {
			$role = 'admin';
		}
		$retArray = array('status' => 'success', 'role' => $role);
	}
	else {
		$retArray = array('status' => 'fail');
	}
	return json_encode($retArray);
}

function _isNamaTokoNotDuplicate($db, $post){
	$namaToko = $post['nama'];
	$arr = array('nama' => $namaToko);
	$json_arr = json_encode($arr);
	$query = json_decode($db->select('toko', $json_arr));

	return sizeof($query->{'data'}) == 0;
}

function _isAllJasaKirimUnique($db, $post){
	$jasaKirim = $post['jasa-kirim'];
	$query = json_decode($db->selectIn('jasa_kirim', 'nama', $jasaKirim));
	return sizeof($query->{'data'}) == sizeof($jasaKirim);
}

function validateCreateToko($db, $post) {
	return _isNamaTokoNotDuplicate($db, $post) && _isAllJasaKirimUnique($db, $post);
}

function createToko($db, $post){
	$retArray = array();

	if(validateCreateToko($db, $post)){
		//masukin toko
		$nama = $post['nama'];
		$deskripsi = $post['deskripsi'];
		$slogan = $post['slogan'];
		$lokasi = $post['lokasi'];
		$email = $post['email'];
		$arr = array('nama' => $nama,
						'deskripsi' => $deskripsi,
						'slogan' => $slogan,
						'lokasi' => $lokasi,
						'email_penjual' => $email);
		$json_arr = json_encode($arr);
		$db->insert('toko', $json_arr);

		//masukin toko_jasa_kirim
		$jasaKirim = $post['jasa-kirim'];
		foreach($jasaKirim as $jasa){
			$arr = array('nama_toko' => $nama,
							'jasa_kirim' => $jasa);
			$json_arr = json_encode($arr);
			$db->insert('toko_jasa_kirim', $json_arr);
		}

		//ubah jadi penjual
		$chgArr = array('is_penjual' => true);
		$condArr = array('email' => $email);
		$db->update('pelanggan', json_encode($chgArr), json_encode($condArr));

		$retArray = array("status" => "success", "msg" => "toko berhasil dibuat");
	} else {
		$retArray = array("status" => "fail", "msg" => "nama atau jasa kirim tidak unik");
	}

	return json_encode($retArray);
}

function getPhoneCredits($db, $post) {
	$offset = $post['offset'] * 10;
	$table = "produk_pulsa";

	$whereCond = json_encode(array("'1'" => '1'));
	$addCond = json_encode(array('limit' => PAGE_LIMIT, 'offset' => $offset));

	$phoneCreds = $db->select($table, $whereCond, $addCond);
	$phoneCredsArray = json_decode($phoneCreds);
	$productCode = array();
	foreach($phoneCredsArray->data as $item){
		array_push($productCode, $item->kode_produk);
	}

	$products = $db->selectIn('produk', 'kode_produk', $productCode);
	$productsArr = json_decode($products);
	foreach($phoneCredsArray->data as $key => $item) {
		$phoneCredsArray->data[$key] = array_merge((array)$item, (array)$productsArr->data[$key]);
	}
	return json_encode($phoneCredsArray);
}

function buyPhoneCredits($db, $post) {
	$productCode = $post['product-code'];
	$number = $post['number'];
	$userEmail = $post['user-email'];

	$now = date('d/m/Y');
	$timestamp = date('Y-m-d h:i:s');

	$whereCond = json_encode(array("kode_produk" => $productCode));
	$totalPayment = json_decode($db->select('produk', $whereCond))->data[0]->harga;
	$nominal = json_decode($db->select('produk_pulsa', $whereCond))->data[0]->nominal;

	$noInvoice = json_decode($db->selectLast('transaksi_pulsa', 'no_invoice'))->data[0]->no_invoice;
	$noInvoice++;

	$json = json_encode(array('tanggal' => $now, 'waktu_bayar' => $timestamp, 'status' => 2, 'total_bayar' => $totalPayment,
				  'email_pembeli' => $userEmail, 'nominal' => $nominal, 'nomor' => $number,
				  'kode_produk' => $productCode, 'no_invoice' => $noInvoice));

	return $db->insert('transaksi_pulsa', $json);
}

function getShops($db, $post) {
	return $db->select('toko', json_encode(array("'1'" => '1')));
}

function getCategory($db, $post) {
	$whereCond = json_encode(array("'1'" => '1'));
	return $db->select('kategori_utama', $whereCond);
}

function getProductsByShop($db, $post) {
	$shopName = $post['shop-name'];
	$offset = $post['offset'] * 10;
	$category = $post['category'];
	$subcategory = $post['subcategory'];

	$whereCondArr = array('nama_toko' => $shopName);
	if(isset($subcategory) && !empty($subcategory)) {
		$whereCondArr = array_merge($whereCondArr, array('kategori' => $subcategory));
	}
	$whereCond = json_encode($whereCondArr);
	$addCond = json_encode(array('limit' => PAGE_LIMIT, 'offset' => $offset));
	return $db->select('shipped_produk', $whereCond, $addCond);
}

function getPhoneCreditTransactions($db, $post) {
	$userEmail = $post['user-email'];
	$offset = $post['offset'] * 10;

	$whereCond = json_encode(array('email_pembeli' => $userEmail));
	$addCond = json_encode(array('limit' => PAGE_LIMIT, 'offset' => $offset));
	$ret = json_decode($db->select('transaksi_pulsa', $whereCond, $addCond));
	$productCodes = array();
	foreach($ret->data as $item){
		array_push($productCodes, $item->kode_produk);
	}
	$products = json_decode($db->selectIn('produk', 'kode_produk', $productCodes));
	$mapProductNames = array();
	foreach($products->data as $item){
		$mapProductNames[$item->kode_produk] = $item->nama;
	}
	foreach($ret->data as $key => $item) {
		$ret->data[$key] = array_merge((array)$item, array('nama_produk' => $mapProductNames[$item->kode_produk]));
	}
	return json_encode($ret);
}

function getShippedTransactions($db, $post) {
	$userEmail = $post['user-email'];
 	$offset = $post['offset'] * 10;
	$whereCond = json_encode(array('email_pembeli' => $userEmail));
	$addCond = json_encode(array('group by' => 'no_invoice', 'limit' => PAGE_LIMIT, 'offset' => $offset));
	return $db->select('transaksi_shipped', $whereCond, $addCond);
}

function getProductsByInvoice($db, $post) {
	$offset = $post['offset'] * 10;
	$noInvoice = $post['invoice'];
	$userEmail = $post['user-email'];

	$whereCond = json_encode(array('no_invoice' => $noInvoice));
	$addCond = json_encode(array('limit' => PAGE_LIMIT, 'offset' => $offset));
	$ret =  json_decode($db->select('list_item', $whereCond, $addCond));
	$productCodes = array();
	foreach($ret->data as $item){
		array_push($productCodes, $item->kode_produk);
	}
	$products = json_decode($db->selectIn('produk', 'kode_produk', $productCodes));
	$mapProductNames = array();
	foreach($products->data as $item){
		$mapProductNames[$item->kode_produk] = $item->nama;
	}
	$ulasan = json_decode(getUlasanByUser($db, $userEmail));
	$mapUlasan = array();
	foreach($ulasan->data as $item) {
		$mapUlasan[$item->kode_produk] = true;
	}
	foreach($ret->data as $key => $item) {
		$ret->data[$key] = array_merge((array)$item, array('nama_produk' => $mapProductNames[$item->kode_produk]));
		if($mapUlasan[$item->kode_produk] == true) {
			$ret->data[$key] = array_merge((array)$ret->data[$key], array('is_ulas' => $mapUlasan[$item->kode_produk]));
		} else {
			$ret->data[$key] = array_merge((array)$ret->data[$key], array('is_ulas' => false));
		}
	}
	return json_encode($ret);
}

function getUlasanByUser($db, $userEmail) {
	$whereCond = json_encode(array('email_pembeli' => $userEmail));
	return $db->select('ulasan', $whereCond);
}

function _isNamaJasaKirimUnique($db, $post){
	$namaTJK = $post['nama'];
	$arr = array('nama' => $namaTJK);
	$json_arr = json_encode($arr);
	$query = json_decode($db->select('jasa_kirim', $json_arr));

	return sizeof($query->{'data'}) == 0;
}

function validateCreateJasaKirim($db, $post) {
	return _isNamaJasaKirimUnique($db, $post);
}

function createJasaKirim($db, $post){
	$retArray = array();

	if(validateCreateJasaKirim($db, $post)){
		$nama = $post['nama'];
		$lama_kirim = $post['lama_kirim'];
		$tarif = $post['tarif'];
		$arr = array('nama' => $nama,
						'lama_kirim' => $lama_kirim,
						'tarif' => $tarif);
		$json_arr = json_encode($arr);
		$db->insert('jasa_kirim', $json_arr);

		$retArray = array("status" => "success", "msg" => "jasa kirim berhasil dibuat");
	} else {
		$retArray = array("status" => "fail", "msg" => "nama jasa kirim tidak unik");
	}

	return json_encode($retArray);
}

function validateCreateUlasan($db, $post) {
	return _isProdukExist($db, $post);
}

function _isProdukExist($db, $post){
	$kode_produk = $post['kode_produk'];
	$arr = array("kode_produk" => $kode_produk);
	$query = json_decode($db->select('produk', json_encode($arr)));
	return sizeof($query->{'data'}) == 1;
}

function createUlasan($db, $post){
	echo "masuk ulasan";
	$retArray = array();

	if(validateCreateUlasan($db, $post)){
		$email_pembeli = $post['email_pembeli'];
		$kode_produk = $post['kode_produk'];
		$rating = $post['rating'];
		$komentar = $post['komentar'];
		$arr = array('email_pembeli' => $email_pembeli,
						'kode_produk' => $kode_produk,
						'rating' => $rating,
						'tanggal' => date("Y/m/d"),
						'komentar' => $komentar);
		$json_arr = json_encode($arr);
		$db->insert('ulasan', $json_arr);

		$retArray = array("status" => "success", "msg" => "Ulasan berhasil dibuat");
	} else {
		$retArray = array("status" => "fail", "msg" => "Ulasan tidak berhasil dibuat");
	}

	return json_encode($retArray);
}

function createPromo($db, $post){
	$retArray = array();
	$deskripsi = $post['deskripsi'];
	$periode_awal = $post['periode_awal'];
	$periode_akhir = $post['periode_akhir'];
	$kode = $post['kode'];
	$id = json_decode($db->selectLast('promo', 'id'))->data[0]->id;
	$id++;
	$arr = array('id' => $id,
					'deskripsi' => $deskripsi,
					'periode_awal' => $periode_awal,
					'periode_akhir' => $periode_akhir,
					'kode' => $kode);
	if ($periode_akhir > $periode_awal) {
		$json_arr = json_encode($arr);
		$db->insert('promo', $json_arr);
		$retArray = array("status" => "success", "msg" => "promo berhasil dibuat");
	}
	else {
		$retArray = array("status" => "fail", "msg" => "promo tidak berhasil");
	}
	addPromoProduk($db, $post);
	return json_encode($retArray);
}

function addPromoProduk($db, $post) {
	$retArray = array();
	$id = json_decode($db->selectLast('promo', 'id'))->data[0]->id;
	$kode_produk = $post['kode_produk'];
	$subKategori = $post['subkategori'];

	$shipped_produk = json_decode($db->select('shipped_produk', json_encode(array('kategori' => $subKategori))));
	foreach($shipped_produk->data as $key => $item) {
		$arr = array('id_promo' => $id,
					 'kode_produk' => $item->kode_produk);

		$json_arr = json_encode($arr);
		$db->insert('promo_produk', $json_arr);	
	}
	$retArray = array("status" => "success", "msg" => "promo produk berhasil");
	return json_encode($retArray);
}


// Create Register Pengguna
function registerPengguna ($db, $post) {
	echo "masuk register pengguna";

	//Masukin registernya
	if(validateRegister($db, $post)) {

		$email = $post['email'];
		$password = $post['password'];
		$namaLengkap = $post['nama'];
		$jenisKelamin = $post['jenis_kelamin'];
		$noTelp = $post['no_telp'];
		$alamat = $post['alamat'];
		/*$arr = array('email' => $email,
						'password' => $password,
						'nama' => $namaLengkap,
						'jenis_kelamin' => $jenisKelamin,
						'tgl_lahir' => date("Y/m/d"),
						'no_telp' => $noTelp,
						'alamat' => $alamat);
		$json_arr = json_encode($arr);
		$db->insert('pengguna', $json_arr);*/
		
		$arr2 = array('email' => $email,
						'is_penjual' => NULL,
						'nilai_reputasi' => 0,
						'poin' => 0);
		$json_arr2 = json_encode($arr2);
		$db->insert('pelanggan', $json_arr2);

		$retArray = array("status" => "success", "msg" => "registrasi berhasil");
	}
	else {
		$retArray = array("status" => "fail", "msg" => "registrasi gagal, mohon ulangi kembali");	
	}
	return json_encode($retArray);
}


function _isEmailNotDuplicate($db, $post){
	$alamatEmail = $post['email'];
	$arr = array('email' => $alamatEmail);
	$json_arr = json_encode($arr);
	$query = json_decode($db->select('pengguna', $json_arr));

	return sizeof($query->{'data'}) == 0;
}

function _isRegisterDataValid($db, $post){
	$password = $post['password'];
	$repPassword = $post['ulang-password'];
	$noTelp = $post['no-telp'];
	$email = $post['email'];
	$namaLengkap = $post['nama'];
	$alamat = $post['alamat'];

	if($noTelp === '') return 0;
	if($email === '') return 0;
	if($namaLengkap === '') return 0;
	if($alamat === '') return 0;
	//check if password not equal or less than 6 char
	if($password != $repPassword || strlen($password) < 6) return 0;
	//check if phone numb all numeric
	if(!is_numeric($noTelp)) return 0;

	return 1;
}

function validateRegister($db, $post) {
	return _isEmailNotDuplicate($db, $post) && _isRegisterDataValid($db, $post);
}

function register($db, $post) {
	$retArray = array();

	if(validateRegister($db, $post)) {
		//add to pengguna
		$email = $post['email'];
		$password = $post['password'];
		$namaLengkap = $post['nama'];
		$jenisKelamin = $post['jenis-kelamin'];
		$noTelp = $post['no-telp'];
		$alamat = $post['alamat'];

		$arr = array('email' => $email,
						'password' => $password,
						'nama' => $namaLengkap,
						'jenis_kelamin' => $jenisKelamin,
						'no_telp' => $noTelp,
						'alamat' => $alamat,
						'tgl_lahir' => '1998/10/10');
		$json_arr = json_encode($arr);
		$db->insert('pengguna', $json_arr);

		//add to pelanggan
		$arr = array('email' => $email,
						'nilai_reputasi' => '00',
						'poin' => '00',
						'is_penjual' => 'false');
		$json_arr = json_encode($arr);
		$db->insert('pelanggan', $json_arr);

		$retArray = array("status" => "success", "msg" => "registrasi berhasil");
	}
	else {
		$retArray = array("status" => "fail", "msg" => "registrasi gagal, ada data yg tidak valid");
	}
	return json_encode($retArray);
}


// /*KATEGORI DAN SUBKATEGORI*/
// // Check if Kode Kategori not Duplicate
function _isKodeKategoriNotDuplicate($db, $post){
	$kodeKategori = $post['kode'];
	$arr = array('kode' => $kodeKategori);
	$json_arr = json_encode($arr);
	$query = json_decode($db->select('kategori_utama', $json_arr));

	return sizeof($query->{'data'}) == 0;
}

// // Check if an array has duplicates
function _has_dupl($array){
	$norm = array();
	foreach($array as $elem){
		array_push($norm, $elem['kode']);
	}
	return count($norm) !== count(array_unique($norm));
}

// // Check if KodeSubkategori Unique
function _isKodeSubkategoriUnique($db, $post){
	$kodeSubkategori = $post['sub-kategori'];
	return !_has_dupl($kodeSubkategori);
}

// // Check if Kode Subkategori not duplicate
function _isKodeSubkategoriNotDuplicate($db, $post){
	$subKategori = $post['sub-kategori'];


	$arr = array();
	foreach($subKategori as $subK){
		array_push($arr, $subK['kode']);
	}
	$query = json_decode($db->selectIn('sub_kategori', 'kode', $arr));
	return sizeof($query->{'data'}) == 0;
}

// Validate kategori utama dan subkategori dengan me-return 3 function diatas (cek KU dan SKU unique or nah)
function validateKategoriSubkategori($db, $post) {
	return _isKodeKategoriNotDuplicate($db, $post) && _isKodeSubkategoriUnique($db, $post) && _isKodeSubkategoriNotDuplicate($db, $post);

}

// // Membuat Kategori Utama dan Subkategori
function createKategoriUtama($db, $post){
	$retArray = array();

	if(validateKategoriSubkategori($db, $post)){
		//masukin Kategori
		$kodeKategori = $post['kode'];
		$namaKategori = $post['nama'];
		$subKategori = $post['sub-kategori'];

		$arr = array('kode' => $kodeKategori,
						'nama' => $namaKategori);
		$json_arr = json_encode($arr);
		$db->insert('kategori_utama', $json_arr);

		//masukin tambah subkategori??
		foreach($subKategori as $subK){
			$arr = array('kode_kategori' => $kodeKategori,
							'kode' => $subK['kode'],
							'nama' => $subK['nama']);
			$json_arr = json_encode($arr);
			$db->insert('sub_kategori', $json_arr);
		}

		$retArray = array("status" => "success", "msg" => "kategori dan subkategori berhasil ditambahkan");
	} else {
		$retArray = array("status" => "fail", "msg" => "kode kategori utama atau subkategori tidak unik!!!");
	}

	return json_encode($retArray);
}

// /*SAMPE SINI*/

function getJasaKirim($db, $get){
	$query = $db->select('jasa_kirim', json_encode(array("1" => "1")));
	return $query;
}

function getSubKategoriForProduk($db, $get){
	$query = $db->select('sub_kategori', json_encode(array("1" => "1")));
	$ret = json_decode($query);
	$res = array();
	foreach($ret->data as $i) {
		$nQuery = json_decode($db->select('kategori_utama', json_encode(array("kode" => $i->kode_kategori))));
		$kategori = $nQuery->data[0]->nama;
		$i->nama_kategori = $kategori;
		array_push($res, $i);
	}
	return json_encode($res);
}


function getSubcategory($db, $post){
	$category = $post['category'];

	$whereCond = json_encode(array('kode_kategori' => $category));

	return $db->select('sub_kategori', $whereCond);
}

function getProduct($db, $post){
	$shop = $post['shop'];
	$subcategory = $post['subcategory'];
	$offset = $post['offset'] * 10;

	$whereCond = json_encode(array('nama_toko' => $shop, 'kategori' => $subcategory));	
	$addCond = json_encode(array('limit' => PAGE_LIMIT, 'offset' => $offset));

	$ret =  json_decode($db->select('shipped_produk', $whereCond, $addCond));

	$productCodes = array();
	foreach($ret->data as $i) {
		array_push($productCodes, $i->kode_produk);
	}
	
	$productNames = json_decode($db->selectIn('produk', 'kode_produk', $productCodes));

	$mapProductNames = array();
	$mapHarga = array();
	$mapDeskripsi = array();
	foreach($productNames->data as $i) {
		$mapProductNames[$i->kode_produk] = $i->nama;
		$mapHarga[$i->kode_produk] = $i->harga;
		$mapDeskripsi[$i->kode_produk] = $i->deskripsi;
	}

	foreach($ret->data as $key => $item) {
		if($mapProductNames[$i->kode_produk]) {
			$ret->data[$key] = array_merge((array)$item, array('nama_produk' => $mapProductNames[$item->kode_produk], 'harga' => $mapHarga[$item->kode_produk], 'deskripsi' => $mapDeskripsi[$item->kode_produk]));
		}
	}

	return json_encode($ret);
}

function validateAddToCart($db, $post) {
	$kuantitas = $post['kuantitas'];

	$shipped_produk = json_decode($db->select('shipped_produk', json_encode(array('kode_produk' => $post['kode_produk']))));

	if($kuantitas > $shipped_produk->data[0]->stok) {
		return False;
	}
	return True;
}

function addToCart($db, $post) {
	if(validateAddToCart($db, $post)) {
		$pembeli = $post['email'];
		$kode_produk = $post['kode_produk'];
		$berat = $post['berat'];
		$kuantitas = $post['kuantitas'];
		$harga = $post['harga'];
		$subtotal = $kuantitas * $harga;

		$arr = array("pembeli" => $pembeli,
					 "kode_produk" => $kode_produk,
					 "berat" => $berat, 
					 "kuantitas" => $kuantitas,
					 "harga" => $harga,
					 "sub_total" => $subtotal);

		$db->insert('keranjang_belanja', json_encode($arr));

		$shipped_produk = json_decode($db->select('shipped_produk', json_encode(array('kode_produk' => $post['kode_produk']))));

		$retArray = array("status" => "success", "msg" => "add to cart berhasil");
	} else {
		$retArray = array("status" => "fail", "msg" => "add to card gagal, stok tidak mencukupi");
	}
	return json_encode($retArray);
}

function seeCart($db, $post) {
	$pembeli = $post['email'];
	$offset = $post['offset'] * 10;

	$whereCond = json_encode(array('pembeli' => $pembeli));

	$addCond = json_encode(array('limit' => PAGE_LIMIT, 'offset' => $offset));

	$ret = json_decode($db->select('keranjang_belanja', $whereCond, $addCond));

	if(sizeof($ret->data) == 0){
		return json_encode($ret);
	}

	$productCodes = array();
	foreach($ret->data as $i) {
		array_push($productCodes, $i->kode_produk);
	}
	
	$productNames = json_decode($db->selectIn('produk', 'kode_produk', $productCodes));

	$mapProductNames = array();
	foreach($productNames->data as $i) {
		$mapProductNames[$i->kode_produk] = $i->nama;
	}

	foreach($ret->data as $key => $item) {
		if($mapProductNames[$i->kode_produk]) {
			$ret->data[$key] = array_merge((array)$item, array('nama_produk' => $mapProductNames[$item->kode_produk]));
		}
	}

	return json_encode($ret);
}

function createShippedTransaction($db, $post) {
	$newInvoice = json_decode($db->selectLast('transaksi_shipped', 'no_invoice'))->data[0]->no_invoice;
	$newInvoice++;
	$now = date('d/m/Y');
	$timestamp = date('Y-m-d h:i:s');
	$pembeli = $post['email'];
	$jasa_kirim = $post['jasa-kirim'];
	$total_bayar = 0;
	$total_berat = 0;

	$carts = json_decode($db->select('keranjang_belanja', json_encode(array('pembeli' => $pembeli))));
	$jasa = json_decode($db->select('jasa_kirim', json_encode(array('nama' => $jasa_kirim))));
	$nama_toko = json_decode($db->select('shipped_produk', json_encode(array('kode_produk' => $carts->data[0]->kode_produk))))->data[0]->nama_toko;
	foreach($carts->data as $key => $item) {
		$total_berat += $item->berat;
		$total_bayar += $item->sub_total;
	}
	$biaya_kirim = $total_berat + $jasa->data[0]->tarif;
	$total_bayar += $biaya_kirim;
	$alamat = $post['alamat'];

	$arr = array('no_invoice' => $newInvoice,
				 'tanggal' => $now, 
				 'status' => 1,
				 'total_bayar' => $total_bayar,
				 'email_pembeli' => $pembeli,
				 'nama_toko' => $nama_toko,
				 'alamat_kirim' => $alamat,
				 'biaya_kirim' => $biaya_kirim,
				 'nama_jasa_kirim' => $jasa_kirim);

	$db->insert('transaksi_shipped', json_encode($arr));

	$retArray = array("status" => "success", "msg" => "shipped produk transaction berhasil");
	return json_encode($retArray);
}


function getKodeProduk($db, $get){
	$kodeProduk = json_decode($db->selectLast('produk', 'kode_produk'))->data[0]->kode_produk;
	$kodeProduk++;
	return json_encode(array('kode-produk' => $kodeProduk));
}

function _isProductIdNotDuplicate($db, $post){
	$kodeProduk = $post['kode-produk'];
	$arr = array('kode_produk' => $kodeProduk);
	$json_arr = json_encode($arr);
	$query = json_decode($db->select('produk', $json_arr));
	return sizeof($query->{'data'}) == 0;
}

function _isCreateProdukPulsaDataValid($db, $post){
	$kodeProduk = $post['kode-produk'];
	$namaProduk = $post['nama-produk'];
	$harga = $post['harga'];
	$deskripsi = $post['deskripsi'];
	$nominal = $post['nominal'];

	if($kodeProduk === '' || $namaProduk === '') return 0;
	if(!is_numeric($harga) || !is_numeric($nominal)) return 0;
	if($deskripsi === '') return 0;

	return 1;
}

function validateCreateProdukPulsa($db, $post) {
	return _isProductIdNotDuplicate($db, $post) && _isCreateProdukPulsaDataValid($db, $post);
}

function createProdukPulsa($db, $post) {
	$retArray = array();

	if(validateCreateProdukPulsa($db, $post)) {
		//add to produk
		$kodeProduk = $post['kode-produk'];
		$namaProduk = $post['nama-produk'];
		$harga = $post['harga'];
		$deskripsi = $post['deskripsi'];
		$nominal = $post['nominal'];
		$arr = array('kode_produk' => $kodeProduk,
						'nama' => $namaProduk,
						'harga' => $harga,
						'deskripsi' => $deskripsi);
		$json_arr = json_encode($arr);
		$db->insert('produk', $json_arr);

		//add to produk_pulsa
		$arr = array('kode_produk' => $kodeProduk,
						'nominal' => $nominal);
		$json_arr = json_encode($arr);
		$db->insert('produk_pulsa', $json_arr);

		$retArray = array("status" => "success", "msg" => "produk pulsa berhasil dimasukkan");
	}
	else {
		$retArray = array("status" => "fail", "msg" => "produk pulsa gagal dimasukkan, ada data yg tidak valid");
	}
	return json_encode($retArray);
}

function _isCreateShippedProdukDataValid($db, $post){
	$kodeProduk = $post['kode-produk'];
	$namaProduk = $post['nama-produk'];
	$harga = $post['harga'];
	$deskripsi = $post['deskripsi'];
	$subKategori = $post['sub-kategori'];
	$barangAsuransi = $post['barang-asuransi'];
	$stok = $post['stok'];
	$barangBaru = $post['barang-baru'];
	$minimalOrder= $post['minimal-order'];
	$minimalGrosir = $post['minimal-grosir'];
	$maksimalGrosir =$post['maksimal-grosir'];
	$hargaGrosir = $post['harga-grosir'];
	$fotoProduk = $post['foto-produk'];

	if($kodeProduk === '' || $namaProduk === '' || $subKategori === '') return 0;
	if($barangBaru != 'f' && $barangBaru !='t') return 0;
	if($barangAsuransi != 'f' && $barangAsuransi !='t') return 0;
	if(!is_numeric($harga) || !is_numeric($stok) || !is_numeric($minimalOrder) ||
		!is_numeric($minimalGrosir) || !is_numeric($maksimalGrosir) || !is_numeric($hargaGrosir)) return 0;
	if($deskripsi === '' || $fotoProduk === '') return 0;

	return 1;

}

function _isEmailExistAndSeller($db, $post){
	$email = $post['email'];
	$data = json_decode($db->select('pelanggan',
							json_encode(array('email' => $email))
							)
						);
	return sizeof($data->{'data'}) != 0 && $data->data[0]->is_penjual === 't';
}


function _isSubkategoriExist($db, $post){
	$subKategori = $post['sub-kategori'];
	$data = json_decode($db->select('sub_kategori',
							json_encode(array('kode' => $subKategori))
							)
						);
	return sizeof($data->{'data'}) != 0;
}


function validateCreateShippedProduk($db, $post){
	return _isProductIdNotDuplicate($db, $post) &&
	 		_isEmailExistAndSeller($db, $post) &&
	 		_isSubkategoriExist($db, $post) &&
	 		_isCreateShippedProdukDataValid($db, $post);
}

function createShippedProduk($db, $post) {
	$retArray = array();
	if(validateCreateShippedProduk($db, $post)) {
		//add to produk
		$kodeProduk = $post['kode-produk'];
		$namaProduk = $post['nama-produk'];
		$harga = $post['harga'];
		$deskripsi = $post['deskripsi'];
		$subKategori = $post['sub-kategori'];
		$barangAsuransi = $post['barang-asuransi'];
		$stok = $post['stok'];
		$barangBaru = $post['barang-baru'];
		$minimalOrder= $post['minimal-order'];
		$minimalGrosir = $post['minimal-grosir'];
		$maksimalGrosir =$post['maksimal-grosir'];
		$hargaGrosir = $post['harga-grosir'];
		$fotoProduk = $post['foto-produk'];
		$email = $post['email'];

		$namaToko = json_decode($db->select('toko',
											json_encode(array('email_penjual' => $email))
											)
								)->data[0]->nama;

		$arr = array('kode_produk' => $kodeProduk,
						'nama' => $namaProduk,
						'harga' => $harga,
						'deskripsi' => $deskripsi);
		$json_arr = json_encode($arr);
		$db->insert('produk', $json_arr);

		//add to shipped_produk
		$arr = array('kategori' => $subKategori,
						'nama_toko' => $namaToko,
						'stok' => $stok,
						'is_asuransi' => $barangAsuransi,
						'is_baru' => $barangBaru,
						'min_order' => $minimalOrder,
						'min_grosir' => $minimalGrosir,
						'max_grosir' => $maksimalGrosir,
						'harga_grosir' => $hargaGrosir,
						'foto' => $fotoProduk,
						'kode_produk' => $kodeProduk);
		$json_arr = json_encode($arr);
		$db->insert('shipped_produk', $json_arr);

		$retArray = array("status" => "success", "msg" => "shipped produk berhasil dimasukkan");
	}
	else {
		$retArray = array("status" => "fail", "msg" => "shipped produk gagal dimasukkan, ada data yg tidak valid");
	}
		return json_encode($retArray);
}


function checkout($db, $post){
	$pembeli = $post['email'];
	$alamat = $post['alamat'];
	$jasa_kirim = $post['jasa-kirim'];

	createShippedTransaction($db, $post);

	$carts = json_decode($db->select('keranjang_belanja', json_encode(array('pembeli' => $pembeli))));

	$noInvoice = json_decode($db->selectLast('transaksi_shipped', 'no_invoice'))->data[0]->no_invoice;

	foreach($carts->data as $key => $item){
		$arr = array('no_invoice' => $noInvoice,
					 'kode_produk' => $item->kode_produk,
					 'berat' => $item->berat,
					 'kuantitas' => $item->kuantitas,
					 'harga' => $item->harga, 
					 'sub_total' => $item->sub_total);
		$db->insert('list_item', json_encode($arr));
		$db->delete('keranjang_belanja', json_encode(array('pembeli' => $pembeli, 'kode_produk' => $item->kode_produk)));
	}
	$retArray = array("status" => "success", "msg" => "checkout berhasil");
	return json_encode($retArray);
}


$db = new DBConnection();


// NOT YET TESTED ON GET
if($_SERVER['REQUEST_METHOD'] == 'GET'){
	$get = $_GET;
	switch ($get['client-request']) {
		case 'get-jasa-kirim':
			echo getJasaKirim($db, $get);
			break;
		case 'get-kode-produk':
			echo getKodeProduk($db, $get);
			break;
		case 'get-sub-kategori-for-produk':
			echo getSubKategoriForProduk($db, $get);
			break;
		default:
			echo 'Welcome to Tokokeren Webserver!';
			break;
	}
} else if($_SERVER['REQUEST_METHOD'] == 'POST'){
	$post = json_decode(file_get_contents("php://input"), true);
	switch ($post['client-request']) {
		case 'login':
			echo login($db, $post);
			break;
		case 'register':
			echo register($db, $post);
			break;
		case 'create-toko':
			echo createToko($db, $post);
			break;
		case 'get-phone-credits':
			echo getPhoneCredits($db, $post);
			break;
		case 'buy-phone-credits':
			echo buyPhoneCredits($db, $post);
			break;
		case 'get-products-by-shop':
			echo getProductsByShop($db, $post);
			break;
		case 'get-shops':
			echo getShops($db, $post);
			break;
		case 'get-phone-credit-transactions':
			echo getPhoneCreditTransactions($db, $post);
			break;
		case 'get-shipped-transactions':
			echo getShippedTransactions($db, $post);
			break;
		case 'get-products-by-invoice':
			echo getProductsByInvoice($db, $post);
			break;
		case 'create-kategori':
			echo createKategoriUtama($db, $post);
			break;
		case 'create-jasa-kirim':
			echo createJasaKirim($db, $post);
			break;
		case 'create-ulasan':
			echo createUlasan($db, $post);
			break;
		case 'register' :
			echo registerPengguna($db, $post);
			break;
		case 'create-produk-pulsa':
			echo createProdukPulsa($db, $post);
			break;
		case 'create-shipped-produk':
			echo createShippedProduk($db, $post);
			break;
		case 'create-promo':
			echo createPromo($db, $post);
			break;
		case 'get-category':
			echo getCategory($db, $post);
			break;
		case 'get-subcategory':
			echo getSubcategory($db, $post);
			break;
		case 'get-product':
			echo getproduct($db, $post);
			break;
		case 'add-to-cart':
			echo addToCart($db, $post);
			break;
		case 'see-cart':
			echo seeCart($db, $post);
			break;
		case 'checkout':
			echo checkout($db, $post);
			break;
		default:
			echo 'Welcome to Tokokeren Webserver!';
			break;
	}
}
?>
