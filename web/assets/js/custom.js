 $(function() {
   $(".button-collapse").sideNav()

   $('.modal').modal();

   $('select').material_select();

    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    $('select').material_select();

    $('#subcategory').click(function() {
      var numChild = 2 + $('.addSubCategory').children().length;
      var K_FIELD_BLUEPRINT = '<p>Subkategori ' + numChild + ':</p>\
                      <div class="row">\
                          <div class="input-field col s12">\
                            <input id="kode-sub-kategori-' + numChild + '" type="text" class="validate kode-sub-kategori" required="" aria-required="true">\
                            <label for="kode-sub-kategori">Kode Sub-Kategori*</label>\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="input-field col s12">\
                            <input id="nama-sub-kategori-' + numChild + '" type="text" class="validate nama-sub-kategori" required="" aria-required="true">\
                            <label for="nama-sub-kategori">Nama Sub-Kategori*</label>\
                          </div>\
                        </div>';
        $('.addSubCategory').append($.parseHTML(K_FIELD_BLUEPRINT));
        $('#lp' + numChild + ' div select').material_select();
    });

    $("#jk-btn").click(function() {
        var nama = $('#jk-nama').val();
        var lama = $('#jk-lama-kirim').val();
        var tarif = $('#jk-tarif').val();
        if (nama != "" && lama != "" && tarif !="") {
            if (lama > 0 && tarif > 0) {
                   var settings = {
                      "async": true,
                      "crossDomain": true,
                      "url": "http://localhost:8000/server/index.php",
                      "method": "POST",
                      "headers": {
                        "content-type": "application/json",
                        "cache-control": "no-cache",
                      },
                      "processData": false,
                      "data": "{\"client-request\": \"create-jasa-kirim\",\n\"nama\": \""+nama+"\",\n\"lama_kirim\": \""+lama+"\",\n\"tarif\": \""+tarif+"\"}"
                    }
                    $.ajax(settings).done(function (response) {
                        json_data = $.parseJSON(response);
                        if(json_data.status == 'success'){
                          sweetAlert("Yeay", "Jasa Kirim berhasil ditambahkan!");
                          $("#modal-jasa-kirim").modal('close')
                        } else {
                          sweetAlert("Oops...", "Jasa kirim gagal dibuat, nama jasa kirim tidak unik.", "error");
                        }

                    });
            } else {
                sweetAlert("Oops...", "Lama kirim dan tarif tidak boleh kurang atau sama dengan 0", "error");
            }

          } else {
            sweetAlert("Oops...", "Tidak boleh ada field yang kosong!", "error");
        }
    });

    $(".logout-btn").click(function() {
      swal({
        title: "Are you sure?",
        text: "See you soon!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Logout",
        closeOnConfirm: false
      },
      function(){
        swal("Logout berhasil", "Bye", "success")
        localStorage.clear()
        $(location).attr('href', 'http://localhost:8000/src/index.html')
      });
    })


 })
