var phone_trans_offset = 0
var shipped_trans_offset = 0
var shipped_list_by_invoice_offset = 0

$(function() {
  setPagination(phone_trans_offset, true)
  fetchShipped(shipped_trans_offset)

	$("a.see-pulsa").click(function() {
		fetchPulsa(phone_trans_offset)
	})
  $("a.see-pulsa").click(function() {
    fetchShipped(shipped_trans_offset)
  })
})

function setPagination(offset, hasNext) {
  $('div#modal-transaction ul.pagination').html('')
  if(offset == 0)
    $('div#modal-transaction ul.pagination').append('<li class="l disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
  else
    $('div#modal-transaction ul.pagination').append('<li class="l"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
  $('div#modal-transaction ul.pagination').append('<li class="waves-effect"><a href="#!">'+(offset+1)+'</a></li>')
  if(hasNext == true)
    $('div#modal-transaction ul.pagination').append('<li class="r waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')
  else 
    $('div#modal-transaction ul.pagination').append('<li class="r disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')

  $('div#modal-transaction ul.pagination li.l').click(function() {
    if($(this).hasClass('disabled')) 
      return
    if($('div#modal-transaction .modal-content .see-pulsa').hasClass('active')) {
      phone_trans_offset--
      fetchPulsa(phone_trans_offset)
    } else {
      shipped_trans_offset--
      fetchShipped(shipped_trans_offset)
    }
  })

  $('div#modal-transaction ul.pagination li.r').click(function() {
    if($(this).hasClass('disabled')) 
      return
    if($('div#modal-transaction .modal-content .see-pulsa').hasClass('active')) {
      phone_trans_offset++
      fetchPulsa(phone_trans_offset)
    } else {
      shipped_trans_offset++
      fetchShipped(shipped_trans_offset)
    }
  })
}

function setPaginationList(invoice, offset, hasNext) {
  $('div#daftar-produk ul.pagination').html('')
  if(offset == 0)
    $('div#daftar-produk ul.pagination').append('<li class="l disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
  else
    $('div#daftar-produk ul.pagination').append('<li class="l"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
  $('div#daftar-produk ul.pagination').append('<li class="waves-effect"><a href="#!">'+(offset+1)+'</a></li>')
  if(hasNext == true)
    $('div#daftar-produk ul.pagination').append('<li class="r waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')
  else 
    $('div#daftar-produk ul.pagination').append('<li class="r disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')

  $('div#daftar-produk ul.pagination li.l').click(function() {
    if($(this).hasClass('disabled')) 
      return
    shipped_list_by_invoice_offset--
    fetchShippedByInvoice(invoice, shipped_list_by_invoice_offset)
  })

  $('div#modal-transaction ul.pagination li.r').click(function() {
    if($(this).hasClass('disabled')) 
      return
    shipped_list_by_invoice_offset++
    fetchShippedByInvoice(invoice, shipped_list_by_invoice_offset)
  })
}

function fetchPulsa(offset) {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "1c372f43-6028-2b19-6849-c259f87fe5c4"
      },
    "processData": false,
    "data": "{\"client-request\": \"get-phone-credit-transactions\",\
    			\n\"user-email\": "+localStorage.getItem('email')+",\
    			\n\"offset\": \""+offset+"\"\
    			}"
  }
  $.ajax(settings).done(function(response) {
  	data = $.parseJSON(response)
    $("div#see-pulsa table tbody").html('')
    jQuery.each(data['data'], function(index, val){
      $("div#see-pulsa table tbody").append('<tr></tr>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['no_invoice']+'</th>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['nama_produk']+'</th>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['tanggal']+'</th>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+(val['status'] == 2 ? 'SUDAH BAYAR' : 'BELUM BAYAR')+'</th>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['total_bayar']+'</th>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['nominal']+'</th>')
      $("div#see-pulsa table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['nomor']+'</th>')
    })
    if(data['data'].length == 10) {
      setPagination(phone_trans_offset, true)
    } else {
      setPagination(phone_trans_offset, false)
    }
  })
}

function fetchShipped(offset) {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "1c372f43-6028-2b19-6849-c259f87fe5c4"
      },
    "processData": false,
    "data": "{\"client-request\": \"get-shipped-transactions\",\
          \n\"user-email\": "+localStorage.getItem('email')+",\
          \n\"offset\": \""+offset+"\"\
          }"
  }
  $.ajax(settings).done(function(response) {
    data = $.parseJSON(response)
    $("div#see-produk table tbody").html('')
    jQuery.each(data['data'], function(index, val){
      $("div#see-produk table tbody").append('<tr></tr>')
      if(val['status'] == 1) {
        status = "transaksi dilakukan"
      } else if(val['status'] == 2) {
        status = "barang dibayar"
      } else if(val['status'] == 3) {
        status = "barang dibayar"
      } else {
        status = "barang diterima"
      }
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['no_invoice']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['nama_toko']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['tanggal']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+status+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['total_bayar']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['alamat_kirim']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['biaya_kirim']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['no_resi']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['nama_jasa_kirim']+'</th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+")").append('<th><a href="#daftar-produk">DAFTAR PRODUK</a></th>')
      $("div#see-produk table tbody tr:nth-child("+(index+1)+") a").click(function() {
        fetchShippedByInvoice(val['no_invoice'], shipped_list_by_invoice_offset)
      })
    })
    if(data['data'].length == 10) {
      setPagination(shipped_trans_offset, true)
    } else {
      setPagination(shipped_trans_offset, false)
    }
  })
}

function fetchShippedByInvoice(invoice, offset) {
  $("div#daftar-produk h6.no-invoice").text("No Invoice : "+invoice)
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "1c372f43-6028-2b19-6849-c259f87fe5c4"
      },
    "processData": false,
    "data": "{\"client-request\": \"get-products-by-invoice\",\
          \n\"invoice\": \""+invoice+"\",\
          \n\"user-email\": "+localStorage.getItem('email')+",\
          \n\"offset\": \""+offset+"\"\
          }"
  }
  $.ajax(settings).done(function(response) {

    data = JSON.parse(response) 

    $("div#daftar-produk table tbody").html('')
    jQuery.each(data['data'], function(index, val){
      $("div#daftar-produk table tbody").append('<tr></tr>')
      $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['nama_produk']+'</th>')
      $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['berat']+'</th>')
      $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['kuantitas']+'</th>')
      $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['harga']+'</th>')
      $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th>'+val['sub_total']+'</th>')
      if(val['is_ulas'] == true){
        $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th><a class="grey-text">Ulas</a></th>')
      } else {
        $("div#daftar-produk table tbody tr:nth-child("+(index+1)+")").append('<th><a class="lime-text" href="#modal-ulas">Ulas</a></th>')
      }
      $("div#daftar-produk table tbody tr:nth-child("+(index+1)+") a").click(function() {
        $("div#modal-ulas form input#ulas-kode-produk").val(val['kode_produk'])

        
        $("#ulas-btn").click(function() {
            var emailtemp = localStorage.getItem('email');
            var email = JSON.parse(emailtemp);
            var kode = val['kode_produk'];
            var rating = $('#ulas-rating').val();
            var komentar = $('#ulas-komentar').val();

            if (kode != "" &&  rating!= "" && komentar !="") {
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "http://localhost:8000/server/index.php",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "1c372f43-6028-2b19-6849-c259f87fe5c4"
                  },
                  "processData": false,
                  "data": "{\"client-request\": \"create-ulasan\",\n\"email_pembeli\": \""+email+"\",\n\"kode_produk\": \""+kode+"\",\n\"rating\": \""+rating+"\",\n\"komentar\": \""+komentar+"\"}"
                }
                sweetAlert("Yeay", "Ulasan berhasil ditambahkan!");
                $.ajax(settings).done(function (response) {
                    $("#modal-ulas").modal('close')
                    $("#daftar-produk").modal('close')
                });
            } else {
                sweetAlert("Oops...", "komentar tidak boleh kosong", "error");
            }
        });
      })
    })

    if(data['data'].length == 10) {
      setPaginationList(invoice, shipped_list_by_invoice_offset, true)
    } else {
      setPaginationList(invoice, shipped_list_by_invoice_offset, false)
    } 
  })
}