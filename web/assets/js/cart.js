var cart_offset = 0
$(function(){
	$("a.see-cart").click(function(){
		fetchCart().then(function(carts) {
			fetchJasaKirim().then(function(jks) {
				setSeeCartBody(carts, jks)
			})
		})
	})
})

function fetchCart() {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"see-cart\",\n\
      			\"offset\": \""+cart_offset+"\",\n\
      			\"email\": "+localStorage.getItem('email')+"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function fetchJasaKirim() {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php?client-request=get-jasa-kirim",
      "method": "GET",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()	
}

function setSeeCartBody(lists, jks) {
	$("div#modal-see-cart table tbody").html('')
	lists.forEach(function(item, index){
		$("div#modal-see-cart table tbody").append('<tr></tr>')
		$("div#modal-see-cart table tbody tr:nth-child("+(index+1)+")").append('<th>'+item['kode_produk']+'</th>')
		$("div#modal-see-cart table tbody tr:nth-child("+(index+1)+")").append('<th>'+item['nama_produk']+'</th>')
		$("div#modal-see-cart table tbody tr:nth-child("+(index+1)+")").append('<th>'+item['berat']+'</th>')
		$("div#modal-see-cart table tbody tr:nth-child("+(index+1)+")").append('<th>'+item['kuantitas']+'</th>')
		$("div#modal-see-cart table tbody tr:nth-child("+(index+1)+")").append('<th>'+item['harga']+'</th>')
		$("div#modal-see-cart table tbody tr:nth-child("+(index+1)+")").append('<th>'+item['sub_total']+'</th>')
	})

	$("div#modal-see-cart select.jasa-kirim").html('')
	jks.forEach(function(item, index) {
		$("div#modal-see-cart select.jasa-kirim").append('<option value="'+item['nama']+'">'+item['nama']+"</option>")
	})

	$("select.jasa-kirim").material_select()

	$("div#modal-see-cart ul.pagination").html('')

	if(cart_offset == 0)
		$('div#modal-see-cart ul.pagination').append('<li class="l disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
	else
		$('div#modal-see-cart ul.pagination').append('<li class="l"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
	$('div#modal-see-cart ul.pagination').append('<li class="waves-effect"><a href="#!">'+(cart_offset+1)+'</a></li>')
	if(lists.length == 10)
		$('div#modal-see-cart ul.pagination').append('<li class="r"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')
	else
		$('div#modal-see-cart ul.pagination').append('<li class="r disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')

	$('div#modal-see-cart ul.pagination li.l').click(function() {
		if($(this).hasClass('disabled')) 
      		return
      	cart_offset--
      	fetchCart().then(function(response) {
			setSeeCartBody(response, jks)
		})
	})

	$('div#modal-see-cart ul.pagination li.r').click(function() {
		if($(this).hasClass('disabled')) 
      		return
      	cart_offset++
      	fetchCart().then(function(response) {
			setSeeCartBody(response, jks)
		})
	})

	$("div#modal-see-cart .modal-action").click(function() {
		alamat = $("div#modal-see-cart input#addr-target").val()
		jasa_kirim = $("div#modal-see-cart select.jasa-kirim").val()

		if($("div#modal-see-cart table tbody tr").length == 0){
			sweetAlert("Oops...", "Tidak ada barang untuk checkout", "error")
			$("#modal-see-cart").modal('close')
			return
		}
		if(alamat == "") {
			sweetAlert("Oops...", "alamat tidak boleh kosong", "error")
			return
		}
		if(jasa_kirim == "") {
			sweetAlert("Oops...", "jasa kirim tidak boleh kosong", "error")
			return
		}
		var settings = {
	      "async": true,
	      "crossDomain": true,
	      "url": "http://localhost:8000/server/index.php",
	      "method": "POST",
	      "headers": {
	        "content-type": "application/json",
	        "cache-control": "no-cache",
	      },
	      "processData": false,
	      "data": "{\"client-request\": \"checkout\",\n\
	      			\"alamat\": \""+alamat+"\",\n\
	      			\"jasa-kirim\": \""+jasa_kirim+"\",\n\
	      			\"email\": "+localStorage.getItem('email')+"\n}"
	    }
	    $.ajax(settings)
	    .done(function (response) {
	    	data = JSON.parse(response)
	    	if(data['status'] == "success") {
				sweetAlert("Yeay", data['msg'], "success")
				$("#modal-see-cart").modal('close')
	    	}
	    	else {
	    		sweetAlert("Oops...", data['msg'], "error")
	    		$("#modal-see-cart").modal('close')
	    	}
	    })
	})
}