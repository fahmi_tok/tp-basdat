  $(function() {
  var role = localStorage.getItem('role');
  if(role !== null){
    if(role === 'admin')
      window.location.replace('http://localhost:8000/src/admin.html');
    else if(role === 'penjual' || role === 'pembeli')
      window.location.replace('http://localhost:8000/src/user.html');
  }

  $("#daftar-btn").click(function() {

    var email = $('#daftar-email').val();
    var password = $('#daftar-password').val();
    var rePassword = $('#daftar-re-password').val();
    var nama = $('#daftar-nama').val();
    var jenisKelamin = $('#daftar-jenis-kelamin').val();
    var noTelp = $('#daftar-no-telp').val();
    var alamat = $('#daftar-alamat').val();

    if ((/(.+)@(.+){1,}\.(.+){1,}/.test(email)) || email=="" || email==null) { } else {
        sweetAlert("Oops...", "Email tidak boleh kosong atau tidak sesuai format", "error");
        return
    }
    if(password == "") {
        sweetAlert("Oops...", "password tidak boleh kosong", "error");
        return 
    }
    if(password != rePassword) {
        sweetAlert("Oops...", "password harus sama", "error");
        return 
    }
    if(jenisKelamin == "") {
        sweetAlert("Oops...", "jenis kelamin tidak boleh kosong", "error");
        return 
    }
    if(noTelp == "") {
        sweetAlert("Oops...", "no telepon tidak boleh kosong", "error");
        return 
    }
    if(alamat == "") {
        sweetAlert("Oops...", "alamat tidak boleh kosong", "error");
        return 
    }
    localStorage.setItem('email', JSON.stringify(email));

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"register\",\n\"email\": \""+email+
                "\",\n\"password\": \""+password+
                "\",\n\"ulang-password\": \""+rePassword+
                "\",\n\"nama\": \""+nama+
                "\",\n \"jenis-kelamin\" : \""+jenisKelamin+
                "\",\n \"no-telp\" : \""+noTelp+
                "\",\n \"alamat\" : \""+alamat+
              "\"\n}"
    }

    $.ajax(settings).done(function (response) {
      data = $.parseJSON(response);
      if (data.status == 'success') {
        sweetAlert("Yeay", "Register successful!");
        localStorage.setItem('email', JSON.stringify(email));
        localStorage.setItem('role', 'pembeli');
        window.location.replace('http://localhost:8000/src/user.html');
        console.log(data);
      }
    });
  });

  $("#login-btn").click(function() {
    var email = $('#login-email').val();
    var password = $('#login-password').val();
    if (email != "" && password !="") {
        console.log(email);
        console.log(password);
        localStorage.setItem('email', JSON.stringify(email));
        var tes = localStorage.getItem('email');
        console.log(tes);
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://localhost:8000/server/index.php",
          "method": "POST",
          "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache",
          },
          "processData": false,
          "data": "{\"client-request\": \"login\",\n\"email\": \""+email+"\",\n\"password\": \""+password+"\"}"
        }

        $.ajax(settings).done(function (response) {
            console.log(response);
            data = $.parseJSON(response);
            console.log(data.status);
          if (data.status == 'success') {
            sweetAlert("Yeay", "Bener");
            console.log(data.role == 'admin')
            if(data.role == 'admin') {
                role = data.role;
                console.log(role);
                localStorage.setItem('role', role);
                window.location.replace('http://localhost:8000/src/admin.html');
            } else {
                role = data.role;
                console.log(role);
                localStorage.setItem('role', role);
                window.location.replace('http://localhost:8000/src/user.html');
            }
          } else {
            sweetAlert("Oops...", "Email atau Password kamu tidak sesuai!", "error");
          }
        });
      } else {
          sweetAlert("Oops...", "Terdapat kesalahan! Mohon ulangi", "error");
      }
  });
})