$(function() {
	var role = localStorage.getItem('role');
	if(role !== null){
		if(role !== 'admin')
			window.location.replace('http://localhost:8000/src/user.html');
	} else
		window.location.replace('http://localhost:8000/src/index.html');

	$(function (){
	  var reqData = {
	    "async": true,
	    "crossDomain": true,
	    "url": "http://localhost:8000/server/index.php?client-request=get-kode-produk",
	    "method": "GET",
	    "headers": {
	      "content-type": "application/json",
	      "cache-control": "no-cache",
	    },
	    "processData": false,
	  };

	  $.ajax(reqData).done(function(response){
	  	var data = $.parseJSON(response);
	  	$('#kode-produk-pulsa').val(data['kode-produk']);
	  });
	});

	$('#add-produk-pulsa-btn').click(function(){
		var kode = $('#kode-produk-pulsa').val();
		var harga = $('#harga-produk-pulsa').val();
		var deskripsi = $('#deskripsi-produk-pulsa').val();
		var nama = $('#nama-produk-pulsa').val();
		var nominal = $('#nominal-produk-pulsa').val();

		if(kode === '' ||
    	deskripsi === '' ||
    	harga === '' || (harga !== '' && parseInt(harga) < 0) ||
    	nama === '' ||
    	nominal === '' || (nominal !== '' && parseInt(nominal) < 0)){
    		var err = '';
    		if(harga !== '' && parseInt(harga) < 0) err += "Field harga tidak boleh negatif.\n";
    		if(nominal !== '' && parseInt(nominal) < 0) err += "Field nominal tidak boleh negatif.\n";
    		if(kode === '') err += "Field kode kosong.\n";
    		if(deskripsi === '') err += "Field deskripsi kosong.\n";
    		if(harga === '') err += "Field harga kosong.\n";
    		if(nominal === '') err += "Field nominal kosong.\n";
	    	sweetAlert("Oops...", err, "error");

	    } else {
	    	var settings = {
	          "async": true,
	          "crossDomain": true,
	          "url": "http://localhost:8000/server/index.php",
	          "method": "POST",
	          "headers": {
	            "content-type": "application/json",
	            "cache-control": "no-cache",
	          },
	          "processData": false,
	          "data": "{\"client-request\": \"create-produk-pulsa\",\n\"nama-produk\": \""+nama+
	                    "\",\n\"deskripsi\": \""+deskripsi+
	                    "\",\n\"nominal\": \""+nominal+
	                    "\",\n \"harga\" : \""+harga+
	                    "\",\n \"kode-produk\" : \""+kode+
	                  "\"\n}"
	        }
	        console.log(settings);
	        $.ajax(settings).done(function (response) {
	        	response = $.parseJSON(response);
	        	if (response.status == 'success') {
		            sweetAlert("Yeay", "Membuat Produk Pulsa successful!");
		            window.location.replace('http://localhost:8000/src/admin.html');
		          } else {
		            sweetAlert("Oops...", "Gagal membuat produk pulsa", "error");
		          }
	        });

	    }
	});

	 $("a.modal-promo").click(function() {
    	fetchCategory().then(function(categories) {
    		isiKategori(categories)
    	})
    })

    $("div#modal-promo select.kategori").on('change', function() {
    	fetchSubcategory($(this).val()).then(function(subcategories) {
    		isiSubkategori(subcategories)
    	})
    })

    $("#promo-btn").click(function() {
        addPromo()
    })

    $("#add-kategori-btn").click(function() {

    var kode = $('#kode-kategori').val();
    var nama = $('#nama-kategori').val();
    var lstSubKategori = [];
    var kodeSubKategori = $('.kode-sub-kategori');
    var namaSubKategori = $('.nama-sub-kategori');


    var subKategoriErr = false;
    var subKategoriErrMsg = '';
    $.each(kodeSubKategori, function(key, val){
    	var kodeSK = val.value;
    	var namaSK = namaSubKategori[key].value;

    	if(kodeSK === '' || kodeSK.length != 5 || namaSK === ''){
    		subKategoriErr = true;
    		if(kodeSK === '') subKategoriErrMsg += "ada Field Kode Subkategori kosong.\n";
    		if(kodeSK.length != 5) subKategoriErrMsg += "ada Field Kode Subkategori yang tidak sepanjang 5 karakter.\n";
    		if(namaSK === '') subKategoriErrMsg += "ada Field Nama Subkategori kosong.\n";
    	}
    	lstSubKategori.push({"kode" : kodeSK, "nama" : namaSK});
    })

    if(nama === '' || kode.length !=3 ||
    	kode === '' ||
    	lstSubKategori === [] || subKategoriErr){
    	var err = subKategoriErrMsg;
    	if(nama === '') err += "Field nama kategori kosong.\n";
    	if(kode.length != 3) err += "Field kode tidak sepanjang 3 karakter.\n";
    	if(kode === '') err += "Field kode kategori kosong.\n";
    	if(lstSubKategori === []) err += "Ada field subkategori yang kosong.\n";
    	sweetAlert("Oops...", err, "error");
    } else {
    	var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://localhost:8000/server/index.php",
          "method": "POST",
          "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache",
          },
          "processData": false,
          "data": "{\"client-request\": \"create-kategori\",\n\"nama\": \""+nama+
                    "\",\n\"kode\": \""+kode+
                    "\",\n \"sub-kategori\" : "+JSON.stringify(lstSubKategori)+
                  "\n}"
        }
        console.log(settings);
        $.ajax(settings).done(function (response) {
        	console.log(response);
      	  data = $.parseJSON(response);
          console.log(data);
          if (data.status == 'success') {
            sweetAlert("Yeay", "Membuat Kategori successful!");
            window.location.replace('http://localhost:8000/src/admin.html');
          } else {
            sweetAlert("Oops...", "Membuat kategori gagal pastikan Kode Subkategori dan Kode Kategori tidak duplicate", "error");
          }
        });

    }
  });

})

function fetchCategory() {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-category\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function fetchSubcategory(category) {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-subcategory\",\
      			\"category\": \""+category+"\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function isiKategori(lists) {
	$("div#modal-promo select.kategori").html("")
	$("div#modal-promo select.kategori").append('<option value="" disabled selected>Kategori</option>')

	lists.forEach(function(item, index) {
		$("div#modal-promo select.kategori").append('<option value="'+item['kode']+'">'+item['nama']+'</option>')		
	})

	$("div#modal-promo select.kategori").material_select()
}

function isiSubkategori(lists) {
	$("div#modal-promo select.subkategori").html("")
	$("div#modal-promo select.subkategori").append('<option value="" disabled selected>subkategori</option>')

	lists.forEach(function(item, index) {
		$("div#modal-promo select.subkategori").append('<option value="'+item['kode']+'">'+item['nama']+'</option>')		
	})

	$("div#modal-promo select.subkategori").material_select()	
}


function addPromo() {
    var deskripsi = $('#promo-deskripsi').val();
    var periode_awal = $('#promo-periode-awal').val();
    var periode_akhir = $('#promo-periode-akhir').val();
    var kode = $('#kode-promo').val();
    var subkategori = $("div#modal-promo select.subkategori").val();
    var startDate = Date.parse(periode_awal);
   	var endDate = Date.parse(periode_akhir);

	if (deskripsi != "" && periode_awal != "" && periode_akhir !="" && kode !="" && subkategori != "") {
		if(startDate < endDate){
			var settings = {
			  "async": true,
			  "crossDomain": true,
			  "url": "http://localhost:8000/server/index.php",
			  "method": "POST",
			  "headers": {
			    "content-type": "application/json",
			    "cache-control": "no-cache",
			  },
			  "processData": false,
			  "data": "{\r\n \"client-request\" : \"create-promo\",\r\n\
			   				 \"deskripsi\" : \""+deskripsi+"\",\r\n\
			   				 \"periode_awal\" : \""+periode_awal+"\",\r\n\
			   				 \"periode_akhir\" : \""+periode_akhir+"\",\r\n\
			   				 \"subkategori\" : \""+subkategori+"\",\r\n\
			   				 \"kode\" : \""+kode+"\"\r\n}"
			}
			$.ajax(settings).done(function (response) {
		  		sweetAlert("Yeay", data['msg'], "success");
		  		$("#modal-promo").modal("close");
			}) 
		} else {
			sweetAlert("Oops...", "Periode awal harus lebih dulu dari periode akhir!", "error");
		} 
  		
	} else {
		sweetAlert("Oops...", "Tidak boleh ada field yang kosong!", "error");
	}
	

}
