$(function() {
	var role = localStorage.getItem('role');
	if(role !== null){
		if(role == 'admin')
			window.location.replace('http://localhost:8000/src/admin.html');
    else if(role == 'penjual'){
      $('.buat-toko').hide();
    }
    else if(role =='pembeli'){
      $('.tambah-produk').hide();
    }
	} else
		window.location.replace('http://localhost:8000/src/index.html');

	//todo : hide menu tambah produk kalo dia pembeli,
	//todo : hide menu tambah toko kalo udah penjual
  var reqData = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/server/index.php?client-request=get-jasa-kirim",
    "method": "GET",
    "headers": {
      "content-type": "application/json",
      "cache-control": "no-cache",
    },
    "processData": false,
  };

  $.ajax(reqData).done(function(response){
    LIST_JASA_KIRIM = '';
    json_data = $.parseJSON(response);
    json_data['data'].forEach(function(data){
      LIST_JASA_KIRIM += '<option value="'+data.nama+'"">'+data.nama+'</option>';
    });

    var blueprint = '<div class="input-field col s12">\
                    <select class="toko-jasa-kirim" id="add-toko-jasa-kirim-1">\
                      <option value="" disabled selected>Jasa Kirim #1</option>'
                      + LIST_JASA_KIRIM +
                    '</select>\
                    <label>Jasa Kirim #1*</label>\
                  </div>';
    $('#toko-jasa-kirim-utama').append($.parseHTML(blueprint));
    $('#toko-jasa-kirim-utama div select').material_select();
  });

  var reqData = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/server/index.php?client-request=get-sub-kategori-for-produk",
    "method": "GET",
    "headers": {
      "content-type": "application/json",
      "cache-control": "no-cache",
    },
    "processData": false,
  };

  $.ajax(reqData).done(function(response){
    LIST_SUB_KATEGORI = '';
    json_data = $.parseJSON(response);
    json_data.forEach(function(data){
      LIST_SUB_KATEGORI += '<option value="'+data.kode+'"">'+data.nama+' dalam '+data.nama_kategori+'</option>';
    });

    var blueprint = '<div class="input-field col s12">\
                    <select class="sub-kategori-shipped-produk-content" id="sub-kategori-shipped-produk-content">\
                      <option value="" disabled selected>Sub Kategori</option>'
                      + LIST_SUB_KATEGORI +
                    '</select>\
                    <label>Sub Kategori*</label>\
                  </div>';
    $('#sub-kategori-shipped-produk').append($.parseHTML(blueprint));
    $('#sub-kategori-shipped-produk div select').material_select();
  });

	$('#add-logistic-partner').click(function() {
	  	var numChild = 2 + $('.additional-logistic-partner').children().length;
	  	var K_FIELD_BLUEPRINT = '<div id="lp' + numChild + '" class="row">\
                  <div class="input-field col s12">\
                    <select class="toko-jasa-kirim" id="add-toko-jasa-kirim-' + numChild + '" >\
                      <option value="" disabled selected>Jasa Kirim #' + numChild + '</option>'
                      + LIST_JASA_KIRIM +
                    '</select>\
                    <label>Jasa Kirim #' + numChild + '</label>\
                  </div>\
                </div>';
        $('.additional-logistic-partner').append($.parseHTML(K_FIELD_BLUEPRINT));
        $('#lp' + numChild + ' div select').material_select();
	  });

    var reqData = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php?client-request=get-kode-produk",
      "method": "GET",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
    };

    $.ajax(reqData).done(function(response){
      var data = $.parseJSON(response);
      $('#kode-shipped-produk').val(data['kode-produk']);
    });

  $("#add-shipped-produk-btn").click(function(){
    var img = $('#shipped-produk-image').prop('files')[0];
    var kode = $('#kode-shipped-produk').val();
    var nama = $('#nama-shipped-produk').val();
    var harga = $('#harga-shipped-produk').val();
    var deskripsi = $('#deskripsi-shipped-produk').val();
    var stok = $('#stok-shipped-produk').val();
    var minimalOrder = $('#min-order-shipped-produk').val();
    var minimalGrosir = $('#min-grosir-shipped-produk').val();
    var maksimalGrosir = $('#max-grosir-shipped-produk').val();
    var hargaGrosir = $('#harga-grosir-shipped-produk').val();
    var subKategori = $('#sub-kategori-shipped-produk-content')[0].value;
    var email = localStorage.getItem('email');
    var isAsuransi = $('[type=radio].asuransi')[0];
    isAsuransi.checked === true ? isAsuransi = 't' : isAsuransi = 'f';
    var isBaru = $('[type=radio].baru')[0].value;
    isBaru.checked === true ? isBaru = 't' : isBaru = 'f';

    console.log(email);
    console.log(isBaru);

    var form = new FormData();
    form.append("image", img);
    form.append("client-request", "post-photo");

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/img-api.php",
      "method": "POST",
      "headers": {
        "cache-control": "no-cache"
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }

    $.ajax(settings).done(function (response) {
      data = $.parseJSON(response);
      if(data.status === 'success'){
        url = data.url;
        if(nama === '' ||
          deskripsi === '' ||
          kode === '' ||

          harga === '' || (harga !== '' && parseInt(harga) < 0) ||
          stok === '' || (stok !== '' && parseInt(stok) < 0) ||
          minimalOrder === '' || (minimalOrder !== '' && parseInt(minimalOrder) < 0) ||
          maksimalGrosir === '' || (maksimalGrosir !== '' && parseInt(maksimalGrosir) < 0) ||
          minimalGrosir === '' || (minimalGrosir !== '' && parseInt(minimalGrosir) < 0) ||
          hargaGrosir === '' || (hargaGrosir !== '' && parseInt(hargaGrosir) < 0) ||
          subKategori === '' ||
          email === '' ||
          isAsuransi === '' ||
          isBaru === ''){

          var err = '';
          if(minimalOrder !== '' && parseInt(minimalOrder) < 0) err += "Field minimal order tidak boleh negatif.\n"
          if(harga !== '' && parseInt(harga) < 0) err += "Field harga tidak boleh negatif.\n"
          if(stok !== '' && parseInt(stok) < 0) err += "Field stok tidak boleh negatif.\n"
          if(maksimalGrosir !== '' && parseInt(maksimalGrosir) < 0) err += "Field maksimal grosir tidak boleh negatif.\n"
          if(minimalGrosir !== '' && parseInt(minimalGrosir) < 0) err += "Field minimal grosir tidak boleh negatif.\n"
          if(hargaGrosir !== '' && parseInt(hargaGrosir) < 0) err += "Field hargaGrosir tidak boleh negatif.\n"
          if(nama === '') err += "Field nama kosong.\n";
          if(deskripsi === '') err += "Field deskripsi kosong.\n";
          if(kode === '') err += "Field kode kosong.\n";
          if(harga === '') err += "Field harga kosong.\n";
          if(stok === '') err += "Field stok kosong.\n";
          if(minimalOrder === '') err += "Field minimal order kosong.\n";
          if(maksimalGrosir === '') err += "Field maksimal grosir kosong.\n";
          if(minimalGrosir === '') err += "Field minimal grosir kosong.\n";
          if(hargaGrosir === '') err += "Field harga grosir kosong.\n";
          if(subKategori === '') err += "Field sub kategori kosong.\n";
          if(email === '') err += "Field email kosong.\n";
          if(isAsuransi === '') err += "Field barang asuransi kosong.\n";
          if(isBaru === '') err += "Field barang baru kosong.\n";

          sweetAlert("Oops...", err, "error");
        } else {
          var settings = {
              "async": true,
              "crossDomain": true,
              "url": "http://localhost:8000/server/index.php",
              "method": "POST",
              "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache",
              },
              "processData": false,
              "data": "{\"client-request\": \"create-shipped-produk\",\n\"nama-produk\": \""+nama+
                        "\",\n\"deskripsi\": \""+deskripsi+
                        "\",\n\"kode-produk\": \""+kode+
                        "\",\n \"harga\" : \""+harga+
                        "\",\n \"sub-kategori\" : \""+subKategori+
                        "\",\n \"barang-asuransi\" : \""+isAsuransi+
                        "\",\n \"stok\" : \""+stok+
                        "\",\n \"barang-baru\" : \""+isBaru+
                        "\",\n \"harga-grosir\" : \""+hargaGrosir+
                        "\",\n \"maksimal-grosir\" : \""+maksimalGrosir+
                        "\",\n \"minimal-grosir\" : \""+minimalGrosir+
                        "\",\n \"minimal-order\" : \""+minimalOrder+
                        "\",\n \"foto-produk\" : \""+url+
                        "\",\n \"email\" : "+localStorage.getItem('email')+
                      "\n}"
          }
          console.log(settings);
          $.ajax(settings).done(function (response) {
            data = $.parseJSON(response);
            if (data.status == 'success') {
              sweetAlert("Yeay", "Membuat shipped produk successful!");
              window.location.replace('http://localhost:8000/src/user.html');
            } else {
              sweetAlert("Oops...", "Membuat shipped produk gagal", "error");
            }
          });
        }
      } else {
        sweetAlert("Oops...", "Gambar produk gagal diupload, silakan coba beberapa saat lagi", "error");
      }
    });

  });

	$("#add-toko-btn").click(function() {

    var nama = $('#add-toko-nama').val();
    var deskripsi = $('#add-toko-deskripsi').val();
    var slogan = $('#add-toko-slogan').val();
    var lokasi = $('#add-toko-lokasi').val();
    var lstJasaKirim = [];
    var jasaKirim = $('select.toko-jasa-kirim');
    var errJasaKirim = '';
    $.each(jasaKirim, function(key, val){
      if(val.value === '') errJasaKirim = 'Field Jasa Kirim ada yang kosong.\n';
    	lstJasaKirim.push(val.value);
    })
    console.log(lstJasaKirim);
    if(nama === '' ||
    	lokasi == '' ||
    	lstJasaKirim === [] || errJasaKirim !== ''){
      var err = errJasaKirim;
    	if (nama === '') err += "Field nama kosong.\n";
      if (lokasi === '') err += "Field lokasi kosong.\n";
      if (lstJasaKirim === []) err += "Field List Jasa Kirim kosong\n";
      sweetAlert("Oops...", err, "error");
    } else {
    	var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://localhost:8000/server/index.php",
          "method": "POST",
          "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache",
          },
          "processData": false,
          "data": "{\"client-request\": \"create-toko\",\n\"nama\": \""+nama+
                    "\",\n\"deskripsi\": \""+deskripsi+
                    "\",\n\"slogan\": \""+slogan+
                    "\",\n \"lokasi\" : \""+lokasi+
                    "\",\n \"jasa-kirim\" : "+JSON.stringify(lstJasaKirim)+
                  	",\n \"email\" : "+localStorage.getItem('email')+
                  "\n}"
        }
        console.log(settings);
        $.ajax(settings).done(function (response) {
        	console.log(response);
          data = $.parseJSON(response);
          if (data.status == 'success') {
            sweetAlert("Yeay", "Membuat Toko successful!");
            localStorage.setItem('role', 'penjual');
            window.location.replace('http://localhost:8000/src/user.html');
            console.log(data);
          } else {
            sweetAlert("Oops...", "Nama toko sudah ada atau ada jasa kirim yang tidak unik", "error");
          }
        });

    }
  });
})