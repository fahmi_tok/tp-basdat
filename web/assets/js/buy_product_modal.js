const STEP_CHOOSE_PRODUCT_TYPE = 1
const STEP_CHOOSE_SHOP = 2
const STEP_CHOOSE_PRODUCT = 3
const STEP_CHOOSE_CREDIT = 4
const STEP_BUY_CREDIT = 5
const STEP_BUY_PRODUCT = 6
const PRODUCT_TYPE_PRODUCT = 1
const PRODUCT_TYPE_CREDIT = 2

var currentStep
var typeProduct
var buy_phone_offset = 0
var shipped_product_offset = 0
var selected_category = ""

$(function(){
	init()

	$("div#modal-buy-product a.next").click(function(){
		typeProduct = $('select.product-type').find(":selected").val();

		if(currentStep == STEP_CHOOSE_PRODUCT_TYPE) {
			if(typeProduct == PRODUCT_TYPE_PRODUCT) {
				currentStep = STEP_CHOOSE_SHOP
			}
			else {
				currentStep = STEP_CHOOSE_CREDIT
			}
		}
		else if(currentStep == STEP_CHOOSE_CREDIT) {
			currentStep = STEP_BUY_CREDIT
		}
		else if(currentStep == STEP_CHOOSE_PRODUCT) {
			currentStep = STEP_BUY_PRODUCT
		}
		else {
			currentStep = currentStep + 1
		}

		if($(this).hasClass("modal-close")) {
			closeBuyProductModal()
		}

		//change content modal body
		if(currentStep == STEP_CHOOSE_SHOP) {
			$("div#modal-buy-product .breadcrumb-steps").append('<a href="#!" class="breadcrumb">Pilih Toko</a>')
			setChooseShopBody()
		} 
		else if(currentStep == STEP_CHOOSE_PRODUCT) {
			$("div#modal-buy-product .breadcrumb-steps").append('<a href="#!" class="breadcrumb">Pilih Produk</a>')	
			$("div#modal-buy-product .modal-action").addClass("modal-close")
			$("div#modal-buy-product .modal-action").text("Selesai")
			fetchCategory().then(function(categories) {
				setChooseProductBody($(".shop-lists").find(".active").text(), categories)	
			})
		}
		else if(currentStep == STEP_CHOOSE_CREDIT) {
			$("div#modal-buy-product .breadcrumb-steps").append('<a href="#!" class="breadcrumb">Beli Pulsa</a>')
			$("div#modal-buy-product .modal-action").text("Beli")
			buy_phone_offset = 0
			setBuyPhoneCreditBody()
		}
		else if(currentStep == STEP_BUY_CREDIT) {
			buyPhoneCredits()
		}
	})
})

function init(){
	currentStep = STEP_CHOOSE_PRODUCT_TYPE
	typeProduct = 0
	buy_phone_offset = 0
	shipped_product_offset = 0
	selected_category = ""
	$("div#modal-buy-product .breadcrumb-steps").html('')
	$("div#modal-buy-product .breadcrumb-steps").append('<a href="#!" class="breadcrumb">Pilih Tipe</a>')
	$("div#modal-buy-product .modal-content").html('')
	$("div#modal-buy-product .modal-content").append(
		'<div class="row">\
          <form class="col s12">\
            <div class="row">\
              <div class="input-field col s12">\
                <select class="product-type">\
                  <option value="" disabled selected>Choose your option</option>\
                  <option value="1">Produk</option>\
                  <option value="2">Pulsa</option>\
                </select>\
                <label>Product Type Select</label>\
              </div>\
            </div>\
          </form>\
        </div>')
	$("select.product-type").material_select()
}

function setBuyPhoneCreditBody(){
	fetchPhoneCredit().then(function(lists) {
		setBuyPhoneCreditLists(lists).then(function(resp) {
			setBuyPhoneCreditForm()
		})
	})
}

function fetchPhoneCredit(){
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-phone-credits\",\n\
      			\"offset\": \""+buy_phone_offset+"\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
        data = JSON.parse(response)
        d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function setBuyPhoneCreditLists(lists){
	d = $.Deferred()
	$("div#modal-buy-product .modal-content").html('')
	$("div#modal-buy-product .modal-content").append('<h5 class="center-align">List Pulsa</h5>')
	$("div#modal-buy-product .modal-content").append('<div class="collection credit-items"></div>')

	$("div#modal-buy-product .modal-content .credit-items").append('<a class="collection-item credit-item lime lighten-2"></a>')
	$("div#modal-buy-product .modal-content .credit-items .credit-item:nth-child(1)").append('<div class="row "></div>')

	$(".credit-item:nth-child(1) .row").append('<div class="col s2 truncate">Kode produk</div>')
	$(".credit-item:nth-child(1) .row").append('<div class="col s2 truncate">Nama Produk</div>')
	$(".credit-item:nth-child(1) .row").append('<div class="col s2 truncate">Harga</div>')
	$(".credit-item:nth-child(1) .row").append('<div class="col s4 truncate">Deskripsi</div>')
	$(".credit-item:nth-child(1) .row").append('<div class="col s2 truncate">Nominal</div>')

	lists.forEach(function(list, index){
		$("div#modal-buy-product .modal-content .credit-items").append('<a class="collection-item credit-item"></a>')
		$("div#modal-buy-product .modal-content .credit-items .credit-item:nth-child("+(index+2)+")").append('<div class="row"></div>')

		$(".credit-item:nth-child("+(index+2)+") .row").append('<div class="col s2 truncate">'+list['kode_produk']+'</div>')
		$(".credit-item:nth-child("+(index+2)+") .row").append('<div class="col s2 truncate">'+list['nama']+'</div>')
		$(".credit-item:nth-child("+(index+2)+") .row").append('<div class="col s2 truncate">'+list['harga']+'</div>')
		$(".credit-item:nth-child("+(index+2)+") .row").append('<div class="col s4 truncate">'+list['deskripsi']+'</div>')
		$(".credit-item:nth-child("+(index+2)+") .row").append('<div class="col s2 truncate">'+list['nominal']+'</div>')
	})

	$("div#modal-buy-product .modal-content").append('<ul class="pagination right-align">')
	if(buy_phone_offset == 0)
		$('div#modal-buy-product ul.pagination').append('<li class="l disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
	else
		$('div#modal-buy-product ul.pagination').append('<li class="l"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
	$('div#modal-buy-product ul.pagination').append('<li class="waves-effect"><a href="#!">'+(buy_phone_offset+1)+'</a></li>')
	if(lists.length == 10)
		$('div#modal-buy-product ul.pagination').append('<li class="r"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')
	else
		$('div#modal-buy-product ul.pagination').append('<li class="r disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')

	$('div#modal-buy-product ul.pagination li.l').click(function() {
		if($(this).hasClass('disabled')) 
      		return
      	buy_phone_offset--
      	setBuyPhoneCreditBody()
	})

	$('div#modal-buy-product ul.pagination li.r').click(function() {
		if($(this).hasClass('disabled')) 
      		return
      	buy_phone_offset++
      	setBuyPhoneCreditBody()
	})

	$(".credit-items").children().each(function(index, item){
		$(item).click(function(){
			if(index == 0) return false
			$(".credit-items").find(".active").removeClass("active")
			$(this).addClass("active")
			$("input#code-product").val($(item).children().children().first().text())
		})
	})
	d.resolve('finish')
	return d.promise()
}


function setBuyPhoneCreditForm(){
	$("div#modal-buy-product .modal-content").append('<div class="row form-buy-credit"></div>')
	$("div#modal-buy-product .modal-content .form-buy-credit").append('<h5 class="center-align">Beli Pulsa</h5>')
	$("div#modal-buy-product .modal-content .form-buy-credit").append('<form class="col s12 form-credit center-align"></form>')
	$(".form-credit").append('<div class="row"><div class="input-field col s6 push-s3 code-product"> <input id="code-product" type="text" class="validate" disabled><label class="active" for="code-product">Kode Produk</label></div></div>')
	$(".form-credit").append('<div class="row"><div class="input-field target-number col s6 push-s3"><input id="target-number" type="text" class="validate"><label class="active" for="target-number">Nomor HP/ Token Listrik</label></div></div>')
}

function buyPhoneCredits(){
	code = $("div#modal-buy-product .form-credit #code-product").val()
	number = $("div#modal-buy-product .form-credit #target-number").val()
	if(code == "") {
		sweetAlert("Oops...", "pulsanya dipilih dulu dong", "error")
		return
	}
	if(number == "") {
		sweetAlert("Oops...", "nomornya dimasukin cuy", "error")
		return
	}
	if(number.length >= 20) {
		sweetAlert("Oops...", "panjang nomor maksimal 20", "error")
		return
	}
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"buy-phone-credits\",\n\
      			\"number\": \""+number+"\",\n\
      			\"product-code\": \""+code+"\",\n\
      			\"user-email\": "+localStorage.getItem('email')+"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
        data = JSON.parse(response)
        sweetAlert("Yeay", "pulsa berhasil dibeli", "success");
        closeBuyProductModal()
    })
}

function closeBuyProductModal(){
	$("div#modal-buy-product .modal-action").removeClass("modal-close")
	$("div#modal-buy-product .modal-action").text("Next")
	$("#modal-buy-product").modal("close")
	init()
}

function setChooseShopBody(){
	$("div#modal-buy-product .modal-content").html('')
	$("div#modal-buy-product .modal-content").append('<h5 class="center-align">List Toko</h5>')
	fetchShop().then(function(lists) {
		$("div#modal-buy-product .modal-content").append('<div class="collection shop-lists" style="margin-bottom:3em;"></div>')	
		
		lists.forEach(function(list, index){
			$("div#modal-buy-product .modal-content .shop-lists").append('<a class="collection-item shop">'+list.nama+'</a>')
		})	


		$(".shop-lists").children().each(function(index, item){
			$(item).click(function(){
				$(".shop-lists").find(".active").removeClass("active")
				$(this).addClass("active")
			})
		})
	})
}

function fetchShop() {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-shops\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
        data = JSON.parse(response)
        d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function fetchCategory() {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-category\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function fetchSubcategory(category) {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-subcategory\",\
      			\"category\": \""+category+"\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()
}

function fetchProduct(shopName, subcategory) {
	d = $.Deferred()
	var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8000/server/index.php",
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
      },
      "processData": false,
      "data": "{\"client-request\": \"get-product\",\n\
      			\"shop\": \""+shopName+"\",\n\
      			\"offset\": \""+shipped_product_offset+"\",\n\
      			\"subcategory\": \""+subcategory+"\"\n}"
    }
    $.ajax(settings)
    .done(function (response) {
    	data = JSON.parse(response)
    	d.resolve(data['data'])
    })
    .fail(d.reject)
    return d.promise()

}

function setChooseProductBody(shopName, categories, subcategories=null, items=null) {
	$("div#modal-buy-product .modal-content").html('')
	$("div#modal-buy-product .modal-content").append('<h5 class="center-align">Daftar Shipped Produk '+shopName+'</h5>')	
	$("div#modal-buy-product .modal-content").append(
		'<div class="row">\
          <form class="col s12">\
            <div class="row">\
               <select class="choose-category">\
                </select>\
            </div>\
            <div class="row">\
               <select class="choose-sub-category">\
                </select>\
            </div>\
          </form>\
        </div>\
        ')
	$("div#modal-buy-product .modal-content").append('<h6 class="center-align filter-produk"><a>Filter</a></h6>')	

	categories.forEach(function(item, index){
		if(selected_category == item['kode']) {
			$("div#modal-buy-product select.choose-category").append('<option value="'+item['kode']+'" selected>'+item['nama']+'</option>')
		} else {
			$("div#modal-buy-product select.choose-category").append('<option value="'+item['kode']+'">'+item['nama']+'</option>')
		}
	})

	$("div#modal-buy-product select.choose-category").change(function() {
		selected_category = $(this).val()
		fetchSubcategory($(this).val()).then(function(response) {
			setChooseProductBody(shopName, categories, response)
		})
	})

	if(subcategories != null) {
		subcategories.forEach(function(item, index){
			$("div#modal-buy-product select.choose-sub-category").append('<option value="'+item['kode']+'">'+item['nama']+'</option>')
		})		
	}


	$("div#modal-buy-product h6.filter-produk").click(function() {
		category = $("div#modal-buy-product select.choose-category").val()
		subcategory = $("div#modal-buy-product select.choose-sub-category").val()
		if(category == null || subcategory == null) {
			sweetAlert("Oops...", "pilih dulu kategori dan subkategorinya", "error")
			return
		}
		fetchProduct(shopName, subcategory).then(function(response) {
			setChooseProductBody(shopName, categories, subcategories, response)
		})
	})

	$("select.choose-category").material_select()
	$("select.choose-sub-category").material_select()
	//fill dummy shipped product

	$("div#modal-buy-product .modal-content").append('\
		<table class="bordered striped highlight shipped-product" style="width:115em; overflow-x:auto;">\
			<thead>\
				<tr class="row">\
					<th class="col s1" style="display:inline-block; width:10em;">Kode Produk</th>\
					<th class="col s1" style="display:inline-block; width:10em;">Nama Produk</th>\
					<th class="col s1" style="display:inline-block; width:10em;">Harga</th>\
					<th class="col s2" style="display:inline-block; width:10em;">Deskripsi</th>\
					<th class="col s1" style="display:inline-block; width:6em;">Is_Asuransi</th>\
					<th class="col s1" style="display:inline-block; width:10em;">Stok</th>\
					<th class="col s1" style="display:inline-block; width:5em;">Is_baru</th>\
					<th class="col s1" style="display:inline-block; width:10em;">Harga Grosir</th>\
					<th class="col s2" style="display:inline-block; width:15em;">berat</th>\
					<th class="col s2" style="display:inline-block; width:15em;"">jumlah</th>\
					<th class="col s1" style="display:inline-block; width:10em;">Action</th>\
				</tr>\
			<thead>\
			<tbody style="width:110em; overflow-x:auto;">\
			</tbody>\
		</table>\
	')

	$("div#modal-buy-product .modal-content").append('<ul class="pagination right-align" style="margin-bottom: 1em">')
	if(shipped_product_offset == 0)
		$('div#modal-buy-product ul.pagination').append('<li class="l disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
	else
		$('div#modal-buy-product ul.pagination').append('<li class="l"><a href="#!"><i class="material-icons">chevron_left</i></a></li>')
	$('div#modal-buy-product ul.pagination').append('<li class="waves-effect"><a href="#!">'+(shipped_product_offset+1)+'</a></li>')
	if(items != null && items.length == 10)
		$('div#modal-buy-product ul.pagination').append('<li class="r"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')
	else
		$('div#modal-buy-product ul.pagination').append('<li class="r disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>')

	$('div#modal-buy-product ul.pagination li.l').off('click').click(function() {
		if($(this).hasClass('disabled')) 
      		return
      	shipped_product_offset--
      	setChooseProductBody(shopName, categories, subcategories, items)
	})

	$('div#modal-buy-product ul.pagination li.r').off('click').click(function() {
		if($(this).hasClass('disabled')) 
      		return
      	shipped_product_offset++
      	setChooseProductBody(shopName, categories, subcategories, items)
	})

	if(items != null) {
		items.forEach(function(item, index) {
			$("div#modal-buy-product table.shipped-product tbody").append('<tr class="row"></tr>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="col s1" style="display:inline-block; width:10em;">'+item["kode_produk"]+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="truncate col s1" style="display:inline-block; width:10em;">'+item["nama_produk"]+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="col s1" style="display:inline-block; width:10em;">'+item["harga"]+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="truncate col s2" style="display:inline-block; width:10em;">'+item["deskripsi"]+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="col s1" style="display:inline-block; width:6em;">'+(item["is_asuransi"]=='t')+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="col s1" style="display:inline-block; width:10em;">'+item["stok"]+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="col s1" style="display:inline-block; width:5em;">'+(item["is_baru"]=='t')+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('<th class="col s1" style="display:inline-block; width:10em;">'+item["harga_grosir"]+'</th>')
			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+")").append('\
				<th class="col s2" style="display:inline-block; width:15em;">\
					<div class="input-field col s6">\
			          <input id="berat" type="number" class="validate">\
			          <label for="berat">berat</label>\
    				</div>\
				</th>\
				<th class="col s2" style="display:inline-block; width:15em;">\
					<div class="input-field col s6">\
			          <input id="jumlah" type="number" class="validate">\
			          <label for="jumlah">jumlah</label>\
    				</div>\
				</th>\
				<th class="col s1" style="display:inline-block; width:10em;">\
					 <button class="btn waves-effect waves-light" type="submit" name="action">Submit<i class="material-icons right">send</i></button>\
				</th>\
			')

			$("div#modal-buy-product table.shipped-product tbody tr:nth-child("+(index+1)+") button").click(function() {
				berat = $("div#modal-buy-product tbody tr:nth-child("+(index+1)+") input#berat").val()
				jumlah = $("div#modal-buy-product tbody tr:nth-child("+(index+1)+") input#jumlah").val()

				if(berat == "") {
					sweetAlert("Oops...", "berat tidak boleh kosong", "error")
					return
				}
				if(jumlah == "") {
					sweetAlert("Oops...", "jumlah tidak boleh kosong", "error")
					return	
				}
				if(parseInt(berat) < 0) {
					sweetAlert("Oops...", "berat tidak boleh kurang dari 0", "error")
					return
				}
				if(parseInt(jumlah) <= 0) {
					sweetAlert("Oops...", "kuantitas minimal 1", "error")
					return	
				}
			
				var settings = {
			      "async": true,
			      "crossDomain": true,
			      "url": "http://localhost:8000/server/index.php",
			      "method": "POST",
			      "headers": {
			        "content-type": "application/json",
			        "cache-control": "no-cache",
			      },
			      "processData": false,
			      "data": "{\"client-request\": \"add-to-cart\",\n\
			      			\"email\": "+localStorage.getItem('email')+",\n\
			      			\"kode_produk\": \""+item["kode_produk"]+"\",\n\
			      			\"berat\": \""+berat+"\",\n\
			      			\"kuantitas\": \""+jumlah+"\",\n\
			      			\"harga\": \""+item['harga']+"\"\n}"
			    }
			    $.ajax(settings)
			    .done(function (response) {
			    	data = JSON.parse(response)
			    	if(data['status'] == "success") {
						sweetAlert("Yeay", data['msg'], "success");			    		
			    	}
			    	else {
			    		sweetAlert("Oops...", data['msg'], "error")
			    	}
			    })
			})
		})
	}
}