insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000000', 'SK001', 'Harris-Jarvis', False, 77, False, 3, 5, 8, 114000, 'https://dummyimage.com/286x134');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000001', 'SK002', 'Moore-Mathews', True, 88, True, 4, 6, 7, 373000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=795×527&w=795&h=527');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000002', 'SK003', 'Owens-Miller', False, 26, True, 2, 3, 11, 300000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=298×926&w=298&h=926');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000003', 'SK004', 'Mathis, Yang and Kirk', False, 82, False, 3, 4, 6, 387000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=279×42&w=279&h=42');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000004', 'SK005', 'Richardson-Schneider', True, 39, True, 5, 7, 15, 482000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=850×482&w=850&h=482');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000005', 'SK006', 'Johnson PLC', False, 86, True, 2, 3, 12, 107000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=768×413&w=768&h=413');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000006', 'SK007', 'Clark, Cole and Barnett', True, 45, False, 1, 3, 10, 359000, 'http://www.lorempixel.com/560/1019');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000007', 'SK008', 'Todd Inc', False, 94, True, 5, 7, 14, 451000, 'http://www.lorempixel.com/975/296');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000008', 'SK009', 'Rowland Ltd', False, 43, False, 4, 5, 15, 48000, 'https://dummyimage.com/830x229');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000009', 'SK010', 'Cummings Inc', True, 10, True, 1, 2, 6, 191000, 'https://dummyimage.com/158x279');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000010', 'SK011', 'Saunders-Green', True, 48, False, 4, 6, 7, 127000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=130×4&w=130&h=4');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000011', 'SK012', 'Robertson Inc', True, 67, False, 3, 5, 14, 146000, 'https://dummyimage.com/298x555');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000012', 'SK013', 'Ferrell Inc', True, 11, True, 2, 4, 11, 291000, 'http://www.lorempixel.com/951/267');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000013', 'SK014', 'Stephens Ltd', False, 46, True, 2, 4, 5, 168000, 'http://www.lorempixel.com/293/968');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000014', 'SK015', 'Harding LLC', True, 12, True, 3, 5, 12, 184000, 'https://dummyimage.com/911x445');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000015', 'SK016', 'Fowler-Hopkins', False, 93, False, 4, 5, 10, 259000, 'http://www.lorempixel.com/900/797');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000016', 'SK017', 'Allen-Thomas', False, 57, False, 3, 4, 13, 210000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=188×649&w=188&h=649');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000017', 'SK018', 'Jimenez Group', False, 40, True, 1, 3, 9, 81000, 'https://dummyimage.com/177x555');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000018', 'SK019', 'Peters LLC', False, 10, False, 1, 2, 5, 30000, 'http://www.lorempixel.com/615/220');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000019', 'SK020', 'Harris and Sons', False, 84, False, 5, 6, 13, 383000, 'https://dummyimage.com/393x765');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000020', 'SK021', 'Coleman Inc', True, 24, False, 5, 6, 8, 282000, 'http://www.lorempixel.com/195/22');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000021', 'SK022', 'Henson Ltd', False, 84, False, 5, 7, 9, 252000, 'https://dummyimage.com/174x636');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000022', 'SK023', 'Vazquez-Fritz', True, 85, False, 1, 2, 4, 41000, 'http://www.lorempixel.com/79/64');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000023', 'SK024', 'Jones-Fowler', False, 48, True, 4, 5, 10, 170000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=571×302&w=571&h=302');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000024', 'SK025', 'Barr, Anderson and Young', False, 18, False, 4, 5, 11, 466000, 'http://www.lorempixel.com/867/932');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000025', 'SK026', 'Guzman Ltd', True, 12, True, 3, 4, 6, 135000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=210×774&w=210&h=774');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000026', 'SK027', 'Gonzalez Ltd', True, 77, True, 3, 4, 7, 183000, 'http://www.lorempixel.com/987/633');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000027', 'SK028', 'Moore, Ortega and Marshall', True, 93, True, 4, 5, 7, 108000, 'https://dummyimage.com/758x495');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000028', 'SK029', 'Cooper-Best', True, 77, False, 3, 5, 15, 471000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=339×493&w=339&h=493');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000029', 'SK030', 'Macias Ltd', True, 92, True, 3, 5, 11, 122000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=980×732&w=980&h=732');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000030', 'SK031', 'Gonzalez LLC', False, 51, True, 2, 4, 7, 289000, 'https://dummyimage.com/531x397');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000031', 'SK032', 'Cain, Nunez and Bowman', False, 18, True, 4, 6, 8, 146000, 'https://dummyimage.com/722x819');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000032', 'SK033', 'Rodriguez Inc', False, 17, False, 3, 4, 12, 414000, 'http://www.lorempixel.com/614/728');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000033', 'SK034', 'Green-Perry', True, 36, True, 2, 4, 14, 166000, 'http://www.lorempixel.com/933/163');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000034', 'SK035', 'Higgins, Collins and Reed', True, 55, False, 1, 3, 4, 185000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=265×607&w=265&h=607');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000035', 'SK036', 'Parker LLC', False, 65, True, 1, 3, 10, 250000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=644×202&w=644&h=202');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000036', 'SK037', 'Bauer-Pearson', True, 26, True, 2, 3, 6, 34000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=830×265&w=830&h=265');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000037', 'SK038', 'Buck-Clayton', True, 62, True, 3, 5, 15, 435000, 'http://www.lorempixel.com/123/678');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000038', 'SK039', 'Johnson, Smith and Nelson', False, 90, True, 1, 3, 6, 158000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=682×740&w=682&h=740');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000039', 'SK040', 'Barton-Boyd', True, 50, False, 1, 3, 11, 31000, 'http://www.lorempixel.com/638/262');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000040', 'SK041', 'Madden-Williams', False, 67, False, 3, 4, 5, 397000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=164×284&w=164&h=284');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000041', 'SK042', 'House, Maldonado and Robinson', True, 52, True, 3, 5, 9, 48000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=768×415&w=768&h=415');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000042', 'SK043', 'Howard Ltd', False, 51, True, 2, 4, 10, 446000, 'https://dummyimage.com/408x1009');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000043', 'SK044', 'Leblanc, White and Marsh', True, 79, True, 5, 6, 10, 171000, 'https://dummyimage.com/444x902');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000044', 'SK045', 'Vincent, Pineda and Ross', True, 47, True, 3, 4, 10, 78000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=70×36&w=70&h=36');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000045', 'SK046', 'Morris Inc', True, 28, True, 4, 6, 13, 430000, 'https://dummyimage.com/933x185');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000046', 'SK047', 'Middleton Inc', False, 36, False, 2, 4, 13, 457000, 'https://dummyimage.com/348x225');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000047', 'SK048', 'Wilson Inc', True, 86, True, 2, 3, 10, 46000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=429×75&w=429&h=75');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000048', 'SK049', 'Smith-Anderson', True, 63, False, 4, 6, 7, 188000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=930×574&w=930&h=574');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000049', 'SK050', 'Anderson, Guerrero and Allen', True, 40, True, 1, 3, 11, 396000, 'https://dummyimage.com/348x981');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000050', 'SK051', 'Rivera-Salinas', True, 79, False, 5, 6, 9, 171000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=951×843&w=951&h=843');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000051', 'SK052', 'Tran-Ellis', True, 77, True, 3, 4, 14, 180000, 'http://www.lorempixel.com/134/823');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000052', 'SK053', 'Moreno Group', False, 37, False, 3, 4, 6, 353000, 'http://www.lorempixel.com/76/237');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000053', 'SK054', 'Cox, Davis and Miller', False, 79, True, 5, 7, 17, 141000, 'http://www.lorempixel.com/48/100');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000054', 'SK055', 'Rogers-Finley', True, 97, True, 3, 4, 9, 458000, 'http://www.lorempixel.com/823/931');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000055', 'SK056', 'Ochoa Inc', False, 12, True, 3, 5, 9, 333000, 'https://dummyimage.com/367x226');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000056', 'SK057', 'Cole, Randolph and Ramos', True, 47, False, 3, 4, 7, 433000, 'https://dummyimage.com/125x604');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000057', 'SK058', 'Trevino-Sims', True, 20, False, 1, 3, 4, 353000, 'https://dummyimage.com/448x37');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000058', 'SK059', 'Patterson Group', True, 39, False, 5, 7, 16, 351000, 'http://www.lorempixel.com/523/431');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000059', 'SK060', 'Wright Group', False, 15, False, 1, 2, 5, 322000, 'https://dummyimage.com/230x343');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000060', 'SK061', 'Shepard, Gonzalez and Freeman', False, 21, True, 2, 3, 13, 366000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=693×316&w=693&h=316');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000061', 'SK062', 'Velazquez-Stevens', False, 31, True, 2, 3, 6, 132000, 'https://dummyimage.com/32x947');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000062', 'SK063', 'Church Ltd', False, 12, False, 3, 5, 11, 426000, 'http://www.lorempixel.com/246/892');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000063', 'SK064', 'Kennedy Group', False, 48, True, 4, 5, 6, 432000, 'http://www.lorempixel.com/29/846');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000064', 'SK065', 'Cole Ltd', True, 57, True, 3, 4, 7, 168000, 'http://www.lorempixel.com/877/918');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000065', 'SK066', 'Crawford, Lee and Mcmillan', True, 60, True, 1, 3, 8, 80000, 'https://dummyimage.com/514x773');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000066', 'SK067', 'Nunez-Mcdowell', True, 15, False, 1, 2, 11, 215000, 'http://www.lorempixel.com/44/698');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000067', 'SK068', 'Brown-Shepard', True, 17, True, 3, 5, 14, 53000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=567×730&w=567&h=730');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000068', 'SK069', 'Collins and Sons', True, 78, False, 4, 5, 8, 477000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=156×818&w=156&h=818');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000069', 'SK070', 'Miller-Mason', True, 92, True, 3, 4, 9, 54000, 'https://dummyimage.com/295x871');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000070', 'SK071', 'Berry-Vega', False, 10, False, 1, 3, 10, 79000, 'http://www.lorempixel.com/633/230');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000071', 'SK072', 'Vargas and Sons', False, 60, False, 1, 2, 8, 471000, 'http://www.lorempixel.com/685/110');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000072', 'SK073', 'Fields Group', True, 16, True, 2, 3, 7, 468000, 'http://www.lorempixel.com/517/355');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000073', 'SK074', 'Gordon LLC', False, 48, True, 4, 5, 7, 472000, 'https://dummyimage.com/974x921');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000074', 'SK075', 'Wilson Group', True, 51, False, 2, 4, 11, 378000, 'http://www.lorempixel.com/954/104');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000075', 'SK076', 'Long Group', False, 90, False, 1, 2, 5, 15000, 'https://dummyimage.com/987x631');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000076', 'SK077', 'Johnson Ltd', False, 16, False, 2, 4, 7, 54000, 'https://dummyimage.com/923x201');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000077', 'SK078', 'Henry PLC', False, 46, True, 2, 4, 14, 290000, 'https://dummyimage.com/126x250');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000078', 'SK079', 'Davis-Adams', True, 37, True, 3, 4, 10, 323000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=13×30&w=13&h=30');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000079', 'SK080', 'Williams-Marshall', True, 36, False, 2, 3, 9, 316000, 'https://dummyimage.com/416x994');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000080', 'SK001', 'Brown-Wilson', True, 77, False, 3, 5, 8, 222000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=160×284&w=160&h=284');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000081', 'SK002', 'Combs, Elliott and Frey', False, 71, False, 2, 4, 10, 400000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=176×795&w=176&h=795');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000082', 'SK003', 'Morrison, Gilbert and Combs', True, 37, True, 3, 5, 7, 211000, 'http://www.lorempixel.com/865/757');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000083', 'SK004', 'Rice LLC', True, 19, True, 5, 7, 9, 345000, 'http://www.lorempixel.com/843/492');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000084', 'SK005', 'Flowers LLC', True, 51, False, 2, 3, 5, 308000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=348×771&w=348&h=771');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000085', 'SK006', 'Perez, Boone and Jenkins', False, 38, True, 4, 5, 9, 358000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=355×130&w=355&h=130');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000086', 'SK007', 'Roberts PLC', True, 89, True, 5, 7, 16, 60000, 'http://www.lorempixel.com/450/820');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000087', 'SK008', 'Pitts Group', True, 52, False, 3, 4, 9, 60000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=807×582&w=807&h=582');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000088', 'SK009', 'Mercer, Cisneros and Smith', True, 79, True, 5, 7, 16, 81000, 'https://dummyimage.com/488x292');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000089', 'SK010', 'Adams Ltd', False, 28, True, 4, 6, 7, 390000, 'http://www.lorempixel.com/109/1019');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000090', 'SK011', 'Norris, Young and Sharp', True, 84, False, 5, 6, 14, 401000, 'https://dummyimage.com/865x46');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000091', 'SK012', 'Lopez, Schaefer and Smith', False, 57, False, 3, 4, 8, 474000, 'https://dummyimage.com/89x435');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000092', 'SK013', 'Peterson, Ray and Ellison', True, 56, True, 2, 4, 5, 70000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=827×886&w=827&h=886');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000093', 'SK014', 'Cordova-Chandler', True, 49, False, 5, 6, 11, 254000, 'https://dummyimage.com/259x246');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000094', 'SK015', 'Preston-Kelly', False, 57, True, 3, 4, 9, 307000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=475×569&w=475&h=569');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000095', 'SK016', 'Mitchell Group', False, 73, True, 4, 5, 8, 488000, 'https://dummyimage.com/142x623');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000096', 'SK017', 'Williams Group', True, 25, False, 1, 2, 8, 492000, 'https://dummyimage.com/474x752');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000097', 'SK018', 'Campbell-Quinn', False, 57, False, 3, 4, 6, 213000, 'https://dummyimage.com/128x808');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000098', 'SK019', 'Moore-Morgan', False, 49, True, 5, 6, 16, 171000, 'https://dummyimage.com/292x650');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000099', 'SK020', 'Hall and Sons', True, 80, False, 1, 3, 7, 182000, 'http://www.lorempixel.com/229/146');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000100', 'SK021', 'Harris-Jarvis', False, 52, True, 3, 5, 15, 185000, 'https://dummyimage.com/534x37');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000101', 'SK022', 'Moore-Mathews', False, 84, False, 5, 7, 8, 53000, 'http://www.lorempixel.com/224/201');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000102', 'SK023', 'Owens-Miller', False, 26, False, 2, 3, 6, 74000, 'https://dummyimage.com/470x763');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000103', 'SK024', 'Mathis, Yang and Kirk', True, 59, True, 5, 7, 13, 390000, 'https://dummyimage.com/631x877');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000104', 'SK025', 'Richardson-Schneider', False, 83, False, 4, 6, 10, 478000, 'https://dummyimage.com/465x282');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000105', 'SK026', 'Johnson PLC', False, 18, True, 4, 5, 7, 203000, 'http://www.lorempixel.com/596/562');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000106', 'SK027', 'Clark, Cole and Barnett', True, 48, True, 4, 5, 12, 14000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=759×883&w=759&h=883');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000107', 'SK028', 'Todd Inc', False, 10, True, 1, 3, 6, 181000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=16×408&w=16&h=408');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000108', 'SK029', 'Rowland Ltd', False, 31, True, 2, 3, 4, 183000, 'http://www.lorempixel.com/285/721');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000109', 'SK030', 'Cummings Inc', False, 33, False, 4, 6, 7, 354000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=6×840&w=6&h=840');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000110', 'SK031', 'Saunders-Green', True, 65, False, 1, 2, 12, 37000, 'http://www.lorempixel.com/576/28');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000111', 'SK032', 'Robertson Inc', True, 88, True, 4, 5, 9, 385000, 'https://dummyimage.com/405x167');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000112', 'SK033', 'Ferrell Inc', False, 96, True, 2, 4, 12, 408000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=402×188&w=402&h=188');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000113', 'SK034', 'Stephens Ltd', False, 43, True, 4, 5, 10, 486000, 'http://www.lorempixel.com/385/539');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000114', 'SK035', 'Harding LLC', True, 37, False, 3, 4, 9, 310000, 'https://dummyimage.com/12x708');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000115', 'SK036', 'Fowler-Hopkins', True, 47, True, 3, 4, 5, 464000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=809×1020&w=809&h=1020');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000116', 'SK037', 'Allen-Thomas', False, 18, True, 4, 5, 7, 451000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=1011×685&w=1011&h=685');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000117', 'SK038', 'Jimenez Group', False, 52, False, 3, 4, 14, 69000, 'http://www.lorempixel.com/464/242');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000118', 'SK039', 'Peters LLC', False, 30, True, 1, 3, 13, 154000, 'http://www.lorempixel.com/1019/819');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000119', 'SK040', 'Harris and Sons', False, 70, False, 1, 2, 6, 261000, 'https://dummyimage.com/131x752');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000120', 'SK041', 'Coleman Inc', True, 64, False, 5, 7, 8, 462000, 'http://www.lorempixel.com/157/172');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000121', 'SK042', 'Henson Ltd', False, 78, True, 4, 6, 14, 133000, 'https://dummyimage.com/908x1020');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000122', 'SK043', 'Vazquez-Fritz', False, 20, True, 1, 3, 4, 335000, 'https://dummyimage.com/722x924');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000123', 'SK044', 'Jones-Fowler', True, 93, False, 4, 5, 7, 48000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=735×529&w=735&h=529');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000124', 'SK045', 'Barr, Anderson and Young', True, 38, True, 4, 6, 8, 293000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=634×977&w=634&h=977');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000125', 'SK046', 'Guzman Ltd', True, 24, True, 5, 6, 7, 459000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=782×332&w=782&h=332');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000126', 'SK047', 'Gonzalez Ltd', False, 71, True, 2, 4, 9, 201000, 'http://www.lorempixel.com/44/959');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000127', 'SK048', 'Moore, Ortega and Marshall', False, 51, False, 2, 4, 5, 112000, 'https://dummyimage.com/749x486');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000128', 'SK049', 'Cooper-Best', True, 63, True, 4, 6, 9, 326000, 'http://www.lorempixel.com/218/700');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000129', 'SK050', 'Macias Ltd', False, 17, False, 3, 4, 6, 312000, 'http://www.lorempixel.com/977/8');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000130', 'SK051', 'Gonzalez LLC', False, 66, False, 2, 4, 12, 34000, 'https://dummyimage.com/294x190');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000131', 'SK052', 'Cain, Nunez and Bowman', True, 74, True, 5, 7, 17, 165000, 'https://dummyimage.com/712x912');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000132', 'SK053', 'Rodriguez Inc', True, 45, False, 1, 2, 11, 456000, 'https://dummyimage.com/251x253');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000133', 'SK054', 'Green-Perry', True, 76, True, 2, 3, 4, 244000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=589×96&w=589&h=96');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000134', 'SK055', 'Higgins, Collins and Reed', False, 10, False, 1, 2, 4, 150000, 'http://www.lorempixel.com/561/973');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000135', 'SK056', 'Parker LLC', True, 87, False, 3, 5, 11, 150000, 'http://www.lorempixel.com/999/160');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000136', 'SK057', 'Bauer-Pearson', False, 12, False, 3, 4, 12, 356000, 'http://www.lorempixel.com/522/712');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000137', 'SK058', 'Buck-Clayton', True, 30, False, 1, 3, 11, 59000, 'http://www.lorempixel.com/884/995');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000138', 'SK059', 'Johnson, Smith and Nelson', True, 28, True, 4, 5, 9, 12000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=574×315&w=574&h=315');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000139', 'SK060', 'Barton-Boyd', False, 76, False, 2, 3, 7, 396000, 'https://dummyimage.com/344x476');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000140', 'SK061', 'Madden-Williams', True, 34, False, 5, 6, 8, 161000, 'http://www.lorempixel.com/222/308');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000141', 'SK062', 'House, Maldonado and Robinson', True, 19, False, 5, 7, 13, 199000, 'http://www.lorempixel.com/539/188');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000142', 'SK063', 'Howard Ltd', False, 79, True, 5, 6, 9, 237000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=37×746&w=37&h=746');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000143', 'SK064', 'Leblanc, White and Marsh', True, 48, True, 4, 6, 8, 365000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=238×882&w=238&h=882');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000144', 'SK065', 'Vincent, Pineda and Ross', True, 37, False, 3, 5, 10, 112000, 'https://dummyimage.com/245x185');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000145', 'SK066', 'Morris Inc', True, 34, True, 5, 6, 14, 365000, 'https://dummyimage.com/114x714');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000146', 'SK067', 'Middleton Inc', True, 24, True, 5, 7, 17, 244000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=862×636&w=862&h=636');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000147', 'SK068', 'Wilson Inc', False, 74, True, 5, 6, 15, 18000, 'http://www.lorempixel.com/123/628');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000148', 'SK069', 'Smith-Anderson', True, 54, True, 5, 7, 9, 175000, 'http://www.lorempixel.com/780/334');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000149', 'SK070', 'Anderson, Guerrero and Allen', True, 23, False, 4, 6, 8, 97000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=444×240&w=444&h=240');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000150', 'SK071', 'Rivera-Salinas', True, 24, False, 5, 6, 15, 337000, 'http://www.lorempixel.com/203/1001');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000151', 'SK072', 'Tran-Ellis', True, 24, False, 5, 6, 12, 294000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=608×381&w=608&h=381');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000152', 'SK073', 'Moreno Group', True, 15, False, 1, 2, 10, 90000, 'http://www.lorempixel.com/848/735');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000153', 'SK074', 'Cox, Davis and Miller', True, 26, False, 2, 4, 8, 132000, 'http://www.lorempixel.com/238/1022');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000154', 'SK075', 'Rogers-Finley', True, 54, False, 5, 6, 12, 214000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=263×20&w=263&h=20');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000155', 'SK076', 'Ochoa Inc', True, 22, True, 3, 4, 7, 226000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=706×707&w=706&h=707');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000156', 'SK077', 'Cole, Randolph and Ramos', False, 25, True, 1, 2, 3, 373000, 'http://www.lorempixel.com/975/554');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000157', 'SK078', 'Trevino-Sims', True, 34, True, 5, 6, 7, 326000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=221×727&w=221&h=727');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000158', 'SK079', 'Patterson Group', False, 36, False, 2, 4, 11, 69000, 'http://www.lorempixel.com/606/880');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000159', 'SK080', 'Wright Group', False, 42, False, 3, 4, 7, 445000, 'https://dummyimage.com/75x906');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000160', 'SK001', 'Shepard, Gonzalez and Freeman', True, 36, False, 2, 4, 5, 244000, 'http://www.lorempixel.com/707/651');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000161', 'SK002', 'Velazquez-Stevens', False, 59, False, 5, 6, 11, 429000, 'http://www.lorempixel.com/412/501');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000162', 'SK003', 'Church Ltd', False, 65, True, 1, 3, 9, 407000, 'https://dummyimage.com/585x649');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000163', 'SK004', 'Kennedy Group', False, 75, True, 1, 3, 4, 500000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=379×393&w=379&h=393');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000164', 'SK005', 'Cole Ltd', True, 93, False, 4, 5, 14, 70000, 'https://dummyimage.com/636x896');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000165', 'SK006', 'Crawford, Lee and Mcmillan', False, 73, False, 4, 5, 8, 466000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=509×332&w=509&h=332');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000166', 'SK007', 'Nunez-Mcdowell', True, 60, True, 1, 3, 12, 258000, 'https://dummyimage.com/157x824');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000167', 'SK008', 'Brown-Shepard', False, 83, True, 4, 5, 14, 377000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=566×936&w=566&h=936');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000168', 'SK009', 'Collins and Sons', False, 83, True, 4, 6, 7, 429000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=291×748&w=291&h=748');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000169', 'SK010', 'Miller-Mason', False, 10, True, 1, 3, 4, 272000, 'https://dummyimage.com/678x563');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000170', 'SK011', 'Berry-Vega', True, 85, True, 1, 3, 10, 107000, 'http://www.lorempixel.com/872/1000');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000171', 'SK012', 'Vargas and Sons', False, 13, True, 4, 5, 13, 251000, 'http://www.lorempixel.com/162/708');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000172', 'SK013', 'Fields Group', False, 81, True, 2, 3, 13, 260000, 'https://dummyimage.com/824x865');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000173', 'SK014', 'Gordon LLC', True, 44, False, 5, 7, 17, 125000, 'http://www.lorempixel.com/792/672');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000174', 'SK015', 'Wilson Group', False, 12, False, 3, 5, 12, 493000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=500×826&w=500&h=826');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000175', 'SK016', 'Long Group', True, 19, False, 5, 7, 17, 445000, 'http://www.lorempixel.com/641/454');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000176', 'SK017', 'Johnson Ltd', False, 34, True, 5, 7, 12, 163000, 'https://dummyimage.com/788x926');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000177', 'SK018', 'Henry PLC', False, 12, True, 3, 4, 11, 77000, 'https://dummyimage.com/49x468');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000178', 'SK019', 'Davis-Adams', True, 22, True, 3, 4, 10, 292000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=545×40&w=545&h=40');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000179', 'SK020', 'Williams-Marshall', True, 53, True, 4, 6, 14, 90000, 'https://dummyimage.com/546x681');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000180', 'SK021', 'Brown-Wilson', True, 61, True, 2, 3, 9, 281000, 'https://dummyimage.com/521x812');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000181', 'SK022', 'Combs, Elliott and Frey', True, 35, False, 1, 2, 11, 89000, 'http://www.lorempixel.com/697/576');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000182', 'SK023', 'Morrison, Gilbert and Combs', True, 93, True, 4, 6, 7, 334000, 'http://www.lorempixel.com/947/215');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000183', 'SK024', 'Rice LLC', True, 24, True, 5, 7, 9, 181000, 'https://dummyimage.com/673x813');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000184', 'SK025', 'Flowers LLC', True, 27, True, 3, 4, 11, 317000, 'http://www.lorempixel.com/145/556');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000185', 'SK026', 'Perez, Boone and Jenkins', True, 20, True, 1, 3, 9, 254000, 'http://www.lorempixel.com/692/891');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000186', 'SK027', 'Roberts PLC', False, 30, True, 1, 2, 7, 307000, 'https://dummyimage.com/13x273');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000187', 'SK028', 'Pitts Group', False, 90, True, 1, 3, 6, 330000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=494×196&w=494&h=196');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000188', 'SK029', 'Mercer, Cisneros and Smith', True, 52, True, 3, 5, 6, 379000, 'http://www.lorempixel.com/573/619');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000189', 'SK030', 'Adams Ltd', True, 89, False, 5, 7, 17, 247000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=511×614&w=511&h=614');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000190', 'SK031', 'Norris, Young and Sharp', False, 23, False, 4, 5, 11, 222000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=326×660&w=326&h=660');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000191', 'SK032', 'Lopez, Schaefer and Smith', True, 48, False, 4, 5, 13, 17000, 'https://dummyimage.com/951x588');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000192', 'SK033', 'Peterson, Ray and Ellison', False, 18, True, 4, 5, 6, 36000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=789×773&w=789&h=773');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000193', 'SK034', 'Cordova-Chandler', True, 66, True, 2, 4, 13, 420000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=478×279&w=478&h=279');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000194', 'SK035', 'Preston-Kelly', False, 14, True, 5, 7, 8, 24000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=756×66&w=756&h=66');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000195', 'SK036', 'Mitchell Group', False, 22, False, 3, 4, 5, 237000, 'https://dummyimage.com/777x632');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000196', 'SK037', 'Williams Group', False, 28, True, 4, 5, 7, 88000, 'https://dummyimage.com/901x90');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000197', 'SK038', 'Campbell-Quinn', False, 69, False, 5, 7, 9, 206000, 'http://www.lorempixel.com/305/399');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000198', 'SK039', 'Moore-Morgan', True, 68, True, 4, 6, 13, 130000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=196×405&w=196&h=405');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000199', 'SK040', 'Hall and Sons', False, 71, True, 2, 4, 10, 159000, 'https://dummyimage.com/300x683');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000200', 'SK041', 'Harris-Jarvis', False, 14, False, 5, 7, 14, 387000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=238×685&w=238&h=685');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000201', 'SK042', 'Moore-Mathews', True, 92, False, 3, 4, 13, 324000, 'http://www.lorempixel.com/367/169');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000202', 'SK043', 'Owens-Miller', True, 51, False, 2, 3, 13, 345000, 'https://dummyimage.com/516x69');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000203', 'SK044', 'Mathis, Yang and Kirk', True, 77, True, 3, 5, 15, 253000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=31×824&w=31&h=824');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000204', 'SK045', 'Richardson-Schneider', True, 64, False, 5, 6, 15, 406000, 'http://www.lorempixel.com/194/487');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000205', 'SK046', 'Johnson PLC', True, 37, False, 3, 4, 9, 406000, 'https://dummyimage.com/533x390');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000206', 'SK047', 'Clark, Cole and Barnett', False, 25, False, 1, 2, 3, 481000, 'http://www.lorempixel.com/136/567');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000207', 'SK048', 'Todd Inc', False, 54, False, 5, 6, 16, 50000, 'https://dummyimage.com/67x858');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000208', 'SK049', 'Rowland Ltd', False, 40, True, 1, 3, 10, 238000, 'http://www.lorempixel.com/898/896');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000209', 'SK050', 'Cummings Inc', True, 36, False, 2, 4, 6, 404000, 'http://www.lorempixel.com/396/24');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000210', 'SK051', 'Saunders-Green', True, 21, False, 2, 4, 10, 454000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=112×386&w=112&h=386');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000211', 'SK052', 'Robertson Inc', True, 18, True, 4, 5, 14, 404000, 'https://dummyimage.com/133x182');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000212', 'SK053', 'Ferrell Inc', True, 47, True, 3, 5, 14, 420000, 'https://dummyimage.com/855x323');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000213', 'SK054', 'Stephens Ltd', False, 38, False, 4, 6, 10, 175000, 'http://www.lorempixel.com/329/659');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000214', 'SK055', 'Harding LLC', True, 13, False, 4, 5, 13, 425000, 'https://dummyimage.com/549x172');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000215', 'SK056', 'Fowler-Hopkins', False, 79, False, 5, 6, 14, 298000, 'http://www.lorempixel.com/698/582');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000216', 'SK057', 'Allen-Thomas', False, 74, True, 5, 6, 10, 108000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=493×201&w=493&h=201');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000217', 'SK058', 'Jimenez Group', False, 42, False, 3, 4, 9, 104000, 'https://dummyimage.com/79x136');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000218', 'SK059', 'Peters LLC', True, 19, False, 5, 6, 15, 152000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=158×471&w=158&h=471');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000219', 'SK060', 'Harris and Sons', False, 93, False, 4, 5, 7, 398000, 'http://www.lorempixel.com/348/617');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000220', 'SK061', 'Coleman Inc', True, 32, True, 3, 4, 13, 108000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=859×589&w=859&h=589');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000221', 'SK062', 'Henson Ltd', True, 86, False, 2, 4, 11, 48000, 'https://dummyimage.com/114x341');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000222', 'SK063', 'Vazquez-Fritz', True, 14, False, 5, 6, 13, 44000, 'http://www.lorempixel.com/879/342');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000223', 'SK064', 'Jones-Fowler', False, 50, True, 1, 3, 6, 122000, 'http://www.lorempixel.com/546/867');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000224', 'SK065', 'Barr, Anderson and Young', False, 65, False, 1, 3, 4, 88000, 'https://dummyimage.com/483x554');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000225', 'SK066', 'Guzman Ltd', True, 83, True, 4, 6, 16, 111000, 'http://www.lorempixel.com/146/51');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000226', 'SK067', 'Gonzalez Ltd', False, 65, True, 1, 2, 12, 114000, 'http://www.lorempixel.com/934/919');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000227', 'SK068', 'Moore, Ortega and Marshall', False, 83, False, 4, 6, 13, 42000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=184×203&w=184&h=203');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000228', 'SK069', 'Cooper-Best', True, 31, False, 2, 3, 8, 387000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=273×602&w=273&h=602');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000229', 'SK070', 'Macias Ltd', False, 96, False, 2, 4, 11, 269000, 'https://dummyimage.com/825x22');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000230', 'SK071', 'Gonzalez LLC', False, 72, True, 3, 4, 7, 400000, 'https://dummyimage.com/570x523');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000231', 'SK072', 'Cain, Nunez and Bowman', True, 73, True, 4, 6, 15, 479000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=881×814&w=881&h=814');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000232', 'SK073', 'Rodriguez Inc', False, 23, True, 4, 5, 13, 64000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=831×945&w=831&h=945');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000233', 'SK074', 'Green-Perry', False, 19, False, 5, 7, 12, 24000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=308×944&w=308&h=944');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000234', 'SK075', 'Higgins, Collins and Reed', True, 41, True, 2, 4, 9, 342000, 'https://dummyimage.com/542x26');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000235', 'SK076', 'Parker LLC', False, 15, True, 1, 2, 4, 274000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=21×483&w=21&h=483');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000236', 'SK077', 'Bauer-Pearson', True, 88, False, 4, 5, 6, 467000, 'http://www.lorempixel.com/165/317');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000237', 'SK078', 'Buck-Clayton', False, 51, True, 2, 3, 8, 128000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=397×969&w=397&h=969');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000238', 'SK079', 'Johnson, Smith and Nelson', True, 82, False, 3, 4, 12, 399000, 'https://dummyimage.com/843x106');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000239', 'SK080', 'Barton-Boyd', False, 52, True, 3, 5, 6, 256000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=932×434&w=932&h=434');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000240', 'SK001', 'Madden-Williams', False, 95, True, 1, 2, 7, 354000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=784×124&w=784&h=124');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000241', 'SK002', 'House, Maldonado and Robinson', True, 70, True, 1, 3, 9, 499000, 'http://www.lorempixel.com/289/333');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000242', 'SK003', 'Howard Ltd', False, 43, True, 4, 5, 10, 17000, 'https://dummyimage.com/596x1023');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000243', 'SK004', 'Leblanc, White and Marsh', False, 44, True, 5, 7, 8, 22000, 'https://dummyimage.com/480x767');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000244', 'SK005', 'Vincent, Pineda and Ross', False, 71, False, 2, 3, 4, 10000, 'http://www.lorempixel.com/1000/720');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000245', 'SK006', 'Morris Inc', False, 85, False, 1, 2, 3, 185000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=816×123&w=816&h=123');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000246', 'SK007', 'Middleton Inc', True, 23, True, 4, 6, 10, 433000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=113×499&w=113&h=499');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000247', 'SK008', 'Wilson Inc', False, 21, False, 2, 3, 5, 109000, 'https://dummyimage.com/493x310');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000248', 'SK009', 'Smith-Anderson', False, 74, True, 5, 6, 9, 30000, 'https://placeholdit.imgix.net/~text?txtsize=55&txt=187×574&w=187&h=574');
insert into shipped_produk(kode_produk, kategori, nama_toko, is_asuransi, stok, is_baru, min_order, min_grosir, max_grosir, harga_grosir, foto)
 values('P0000249', 'SK010', 'Anderson, Guerrero and Allen', True, 20, False, 1, 3, 5, 448000, 'http://www.lorempixel.com/616/169');
