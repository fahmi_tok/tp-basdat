import random

def random_date():
	d = random.randint(1, 28)
	m = random.randint(1, 12)
	y = random.randint(1960, 2010)

	return (str(d)+"/"+str(m)+"/"+str(y))

def random_date():
	d = random.randint(1, 28)
	m = random.randint(1, 12)
	y = random.randint(1960, 2010)

	return (str(d)+"/"+str(m)+"/"+str(y))

def add_date(rand_date):
	return rand_date[:-4] + '2011';

code = ["6011117275333090","210095046032039","4683954491048","561242549716","869904913707942"]
desc = ["optimize seamless markets","enable back-end e-commerce","synthesize out-of-the-box platforms","e-enable world-class niches","morph plug-and-play models"]

for i in range(0, 5):
	pid = 'V' + str(i+1).zfill(5)
	periode_awal = random_date()
	periode_akhir = add_date(periode_awal)
	print("insert into promo(id, deskripsi, periode_awal, periode_akhir, kode)\n VALUES ('{}', '{}', '{}', '{}', '{}');".format(pid, desc[i], periode_awal, periode_akhir, code[i]))