import random

def gen_code_prod(index):
	return "P"+str(index).zfill(7)

def gen_price():
	return str(random.randint(1, 50))+"0"

for i in range(0, 200):
	print("insert into PRODUK_PULSA(kode_produk, nominal)\n values('{}', '{}');".format(gen_code_prod(i), gen_price()))