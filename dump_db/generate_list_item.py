import random

def gen_inv(index):
	return "INV"+str(index%500).zfill(7)

def gen_prod_code(index):
	if(index >= 500):
		adder = 1
	else:
		adder = 0;
	return "P"+str((index+adder)%250).zfill(7)

def gen_berat():
	return random.randint(1, 5)

def gen_kuantitas():
	return random.randint(1, 3)

def gen_price():
	return str(random.randint(1, 50))+"0000"

for i in range(0, 1000):
	berat = gen_berat()
	kuantitas = gen_kuantitas()
	harga = gen_price()

	subtotal = int(harga) * int(kuantitas)

	print("insert into list_item(no_invoice, kode_produk, berat, kuantitas, harga, sub_total)\n VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(gen_inv(i), gen_prod_code(i), berat, kuantitas, harga, subtotal))