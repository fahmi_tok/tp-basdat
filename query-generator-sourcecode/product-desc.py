from faker import Faker

fake = Faker()

print('[', end='')
for i in range(0, 250):
	print('"', end='')
	while(True):
		desc = fake.paragraph(nb_sentences=3, variable_nb_sentences=True);
		break;

	print(desc, end='')
	print('"', end='')
	if i != 249 :
		print(',', end='')
	else :
		print (']', end='')