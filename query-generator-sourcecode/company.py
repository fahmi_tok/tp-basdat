from faker import Faker

fake = Faker()
dic = {}

print('[', end='')
for i in range(0, 100):
	print('"', end='')
	while(True):
		company = fake.company();
		if(len(company) <= 100 and not company in dic):
			dic[company] = False;
			break;

	print(company, end='')
	print('"', end='')
	if i != 99 :
		print(',', end='')
	else :
		print (']', end='')