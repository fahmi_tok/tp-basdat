from faker import Faker

fake = Faker()

print('[', end='')
for i in range(0, 100):
	print('"', end='')
	while(True):
		slogan = fake.catch_phrase();
		if(len(slogan) <= 100):
			break;

	print(slogan, end='')
	print('"', end='')
	if i != 99 :
		print(',', end='')
	else :
		print (']', end='')