from faker import Faker

fake = Faker()
dic = {}

print('[', end='')
for i in range(0, 355):
	print('"', end='')
	while(True):
		email = fake.email();
		if(len(email) <= 50 and not email in dic):
			dic[email] = True;
			break;

	print(email, end='')
	print('"', end='')
	if i != 354 :
		print(',', end='')
	else :
		print (']', end='')