from faker import Faker

fake = Faker()
dic = {}

print('[', end='')
for i in range(0, 5):
	print('"', end='')
	while(True):
		promo_code = fake.credit_card_number(card_type=None)
		if(len(promo_code) <= 20 and not promo_code in dic):
			dic[promo_code] = True;
			break;

	print(promo_code, end='')
	print('"', end='')
	if i != 4 :
		print(',', end='')
	else :
		print (']', end='')