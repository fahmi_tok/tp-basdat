from faker import Faker

fake = Faker()

print('[', end='')
for i in range(0, 355):
	print('"', end='')
	while(True):
		name = fake.name();
		if(len(name) <= 100):
			break;

	print(name, end='')
	print('"', end='')
	if i != 354 :
		print(',', end='')
	else :
		print (']', end='')