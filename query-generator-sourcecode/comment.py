from faker import Faker

fake = Faker()

print('[', end='')
for i in range(0, 50):
	print('"', end='')

	comment = fake.text(max_nb_chars=200)

	print(comment, end='')
	print('"', end='')
	if i != 49 :
		print(',', end='')
	else :
		print (']', end='')
