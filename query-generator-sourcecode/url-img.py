from faker import Faker

fake = Faker()

print('[', end='')
for i in range(0, 250):
	print('"', end='')
	while(True):
		url = fake.image_url(width=None, height=None)
		if(len(url) <= 100):
			break;

	print(url, end='')
	print('"', end='')
	if i != 249 :
		print(',', end='')
	else :
		print (']', end='')