from faker import Faker

fake = Faker()
dic = {}

print('[', end='')
for i in range(0, 250):
	print('"', end='')
	while(True):
		prod = fake.sentence(nb_words=2, variable_nb_words=True)
		if(len(prod) <= 100 and not prod in dic):
			dic[prod] = True;
			break;

	print(prod, end='')
	print('"', end='')
	if i != 249 :
		print(',', end='')
	else :
		print (']', end='')