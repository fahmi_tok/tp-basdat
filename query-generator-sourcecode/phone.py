from faker import Faker

fake = Faker()

print('[', end='')
for i in range(0, 355):
	print('"', end='')
	while(True):
		phone = fake.phone_number();
		if(len(phone) <= 20):
			break;

	print(phone, end='')
	print('"', end='')
	if i != 354 :
		print(',', end='')
	else :
		print (']', end='')