from faker import Faker
from random import Random

fake = Faker()
random = Random()

def generate_phone():
	st = "08"
	for j in range(0, 12):
		st += chr(random.randint(0, 9) + ord('0'))
	return st

def generate_electric():
	st = ""
	for j in range(0, 20):
		st += chr(random.randint(0, 9) + ord('0'))
	return st

print('[', end='')
for i in range(0, 1000):
	print('"', end='')
	if(i % 2 == 1):
		num = generate_phone()
	else:
		num = generate_electric()

	print(num, end='')
	print('"', end='')
	if i != 999 :
		print(',', end='')
	else :
		print (']', end='')