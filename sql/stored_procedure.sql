--procedure to update stock
create or replace function auto_upd_stock()
returns trigger as
$$
	declare
	begin
		if(TG_OP = 'INSERT') then
			update shipped_produk S
			set stok = S.stok - NEW.kuantitas
			where S.kode_produk = NEW.kode_produk;
		elsif (TG_OP = 'DELETE') then
			update shipped_produk S
			set stok = S.stok + OLD.kuantitas
			where S.kode_produk = OLD.kode_produk;
		else
			update shipped_produk S
			set stok = S.stok + OLD.kuantitas
			where S.kode_produk = OLD.kode_produk;

			update shipped_produk S
			set stok = S.stok - NEW.kuantitas
			where S.kode_produk = NEW.kode_produk;
		end if;
		return NEW;
	end;
$$
language plpgsql;

--trigger to call update stock procedure
create trigger auto_upd_stock_trigger
after insert or delete or update on list_item
for each row
execute procedure auto_upd_stock();


--procedure to update poin
create or replace function auto_upd_point()
returns trigger as
$$
	declare
	begin
		if(TG_OP = 'UPDATE') then
			if(NEW.status = 4 and OLD.status <> 4) then
				update pelanggan P
				set poin = P.poin + (NEW.total_bayar / 100)
				where P.email = NEW.email_pembeli;
			end if;
		end if;
		return NEW;
	end;
$$
language plpgsql;

--trigger to call update point procedure
create trigger auto_upd_point
after update on transaksi_shipped
for each row
execute procedure auto_upd_point();
