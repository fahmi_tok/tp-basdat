CREATE SCHEMA TOKOKEREN;

SET search_path TO TOKOKEREN;

CREATE TABLE PENGGUNA (email varchar(50) NOT NULL, password varchar(20) NOT NULL, nama varchar(100) NOT NULL, jenis_kelamin char(1) CHECK (jenis_kelamin='L' or jenis_kelamin='P'), tgl_lahir DATE NOT NULL, no_telp varchar(20) NOT NULL, alamat TEXT NOT NULL, PRIMARY KEY (email));

CREATE TABLE PELANGGAN (email VARCHAR(50) NOT NULL, is_penjual BOOLEAN, nilai_reputasi NUMERIC(10, 1), poin INT, PRIMARY KEY (email), FOREIGN KEY (email) REFERENCES PENGGUNA(email));

CREATE TABLE TOKO (nama varchar(100) NOT NULL, deskripsi text, slogan varchar(100), lokasi text NOT NULL, email_penjual varchar(50) NOT NULL, PRIMARY KEY (nama), FOREIGN KEY (email_penjual) REFERENCES PELANGGAN(email));

CREATE TABLE JASA_KIRIM (nama VARCHAR(100) NOT NULL, lama_kirim VARCHAR(10) NOT NULL, tarif NUMERIC(10, 2) NOT NULL, PRIMARY KEY (nama));

CREATE TABLE TOKO_JASA_KIRIM (nama_toko varchar(100) NOT NULL, jasa_kirim varchar(100) NOT NULL, PRIMARY KEY(nama_toko, jasa_kirim), FOREIGN KEY(nama_toko) REFERENCES TOKO(nama), FOREIGN KEY(jasa_kirim) REFERENCES JASA_KIRIM(nama));

CREATE TABLE KATEGORI_UTAMA (kode CHAR(3) NOT NULL, nama VARCHAR(100) NOT NULL, PRIMARY KEY(kode));

CREATE TABLE SUB_KATEGORI (kode char(5) NOT NULL, kode_kategori char(3) NOT NULL, nama varchar(100) NOT NULL,  PRIMARY KEY(kode), FOREIGN KEY(kode_kategori) REFERENCES KATEGORI_UTAMA(kode));

CREATE TABLE PRODUK (kode_produk CHAR(8) NOT NULL, nama VARCHAR(100) NOT NULL, harga NUMERIC(10, 2) NOT NULL, deskripsi TEXT, PRIMARY KEY(kode_produk));

CREATE TABLE PRODUK_PULSA (kode_produk CHAR(8) NOT NULL, nominal INT NOT NULL, PRIMARY KEY(kode_produk), FOREIGN KEY(kode_produk) REFERENCES PRODUK(kode_produk));

CREATE TABLE SHIPPED_PRODUK(kode_produk CHAR(8) NOT NULL, kategori CHAR(5) NOT NULL, nama_toko VARCHAR(100) NOT NULL, is_asuransi BOOLEAN NOT NULL, stok INT NOT NULL, is_baru BOOLEAN NOT NULL, min_order INT NOT NULL, min_grosir INT NOT NULL, max_grosir INT NOT NULL, harga_grosir NUMERIC(10, 2) NOT NULL, foto VARCHAR(100) NOT NULL, PRIMARY KEY(kode_produk), FOREIGN KEY(kode_produk) REFERENCES PRODUK(kode_produk), FOREIGN KEY(kategori) REFERENCES SUB_KATEGORI(kode), FOREIGN KEY(nama_toko) REFERENCES TOKO(nama));

CREATE TABLE TRANSAKSI_SHIPPED (no_invoice CHAR(10) NOT NULL, tanggal DATE NOT NULL, waktu_bayar TIMESTAMP, status SMALLINT CHECK(status='1' or status='2' or status='3' or status='4'), total_bayar NUMERIC(10, 2) NOT NULL, email_pembeli varchar(50) NOT NULL, nama_toko varchar(100) NOT NULL, alamat_kirim TEXT NOT NULL, biaya_kirim NUMERIC(10, 2) NOT NULL, no_resi char(16), nama_jasa_kirim varchar(16) NOT NULL, PRIMARY KEY(no_invoice), FOREIGN KEY(email_pembeli) REFERENCES PELANGGAN(email), FOREIGN KEY(nama_toko, nama_jasa_kirim) REFERENCES TOKO_JASA_KIRIM(nama_toko, jasa_kirim));

CREATE TABLE TRANSAKSI_PULSA(no_invoice CHAR(10) NOT NULL, tanggal DATE NOT NULL, waktu_bayar TIMESTAMP, status SMALLINT CHECK(status='1' or status='2'), total_bayar NUMERIC(10, 2) NOT NULL, email_pembeli VARCHAR(50) NOT NULL, nominal INT NOT NULL, nomor VARCHAR(20) NOT NULL, kode_produk CHAR(8) NOT NULL, PRIMARY KEY(no_invoice), FOREIGN KEY(email_pembeli) REFERENCES PELANGGAN(email), FOREIGN KEY(kode_produk) REFERENCES PRODUK_PULSA(kode_produk));

CREATE TABLE PROMO(id CHAR(6) NOT NULL, deskripsi TEXT NOT NULL, periode_awal DATE NOT NULL, periode_akhir DATE NOT NULL, kode varchar(20) NOT NULL, PRIMARY KEY(id));

CREATE TABLE PROMO_PRODUK(id_promo CHAR(6), kode_produk CHAR(8), PRIMARY KEY(id_promo, kode_produk), FOREIGN KEY(id_promo) REFERENCES PROMO(id), FOREIGN KEY(kode_produk) REFERENCES PRODUK(kode_produk));

CREATE TABLE ULASAN(email_pembeli varchar(50) NOT NULL, kode_produk char(8) NOT NULL, tanggal DATE NOT NULL, rating INT NOT NULL, komentar TEXT, PRIMARY KEY(email_pembeli, kode_produk), FOREIGN KEY(email_pembeli) REFERENCES PELANGGAN(email), FOREIGN KEY(kode_produk) REFERENCES SHIPPED_PRODUK(kode_produk));

CREATE TABLE KOMENTAR_DISKUSI(pengirim VARCHAR(50), penerima VARCHAR(50), waktu TIMESTAMP, komentar TEXT NOT NULL, PRIMARY KEY(pengirim, penerima, waktu), FOREIGN KEY(pengirim) REFERENCES PELANGGAN(email), FOREIGN KEY(penerima) REFERENCES PELANGGAN(email));

CREATE TABLE LIST_ITEM(no_invoice char(10) NOT NULL, kode_produk char(8) NOT NULL, berat INT NOT NULL, kuantitas INT NOT NULL, harga NUMERIC(10, 2) NOT NULL, sub_total NUMERIC(10, 2) NOT NULL, PRIMARY KEY(no_invoice, kode_produk), FOREIGN KEY (no_invoice) REFERENCES TRANSAKSI_SHIPPED(no_invoice), FOREIGN KEY(kode_produk) REFERENCES SHIPPED_PRODUK(kode_produk));

CREATE TABLE KERANJANG_BELANJA(pembeli varchar(50) NOT NULL, kode_produk char(8) NOT NULL, berat INT NOT NULL, kuantitas INT NOT NULL, harga NUMERIC(10, 2) NOT NULL, sub_total NUMERIC(10, 2) NOT NULL, PRIMARY KEY(pembeli, kode_produk), FOREIGN KEY(pembeli) REFERENCES PELANGGAN(email), FOREIGN KEY(kode_produk) REFERENCES SHIPPED_PRODUK(kode_produk));